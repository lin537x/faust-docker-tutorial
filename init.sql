
DROP DATABASE IF EXISTS test_db;

CREATE DATABASE test_db OWNER docker;

GRANT ALL PRIVILEGES ON DATABASE test_db TO docker;

\c test_db;

DROP TABLE IF EXISTS user_info;

CREATE TABLE user_info(user_id SERIAL PRIMARY KEY, user_name varchar(50) NOT NULL, risk_score real);

insert into user_info(user_name, risk_score) values ('Admin', 1.2), ('User', 0.9);

/* select * from user_info; */

