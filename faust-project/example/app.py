import faust
import logging
from simple_settings import settings
from logging.config import dictConfig

import asyncpg
from .database import get_engine, get_session
from sqlalchemy.sql import text

logger = logging.getLogger(__name__)

app = faust.App(
    version=1,
    autodiscover=True,
    origin='example',
    id=settings.FAUST_ID,  # https://faust.readthedocs.io/en/latest/userguide/settings.html
    # id="1",  # https://faust.readthedocs.io/en/latest/userguide/settings.html
    broker=settings.KAFKA_BOOTSTRAP_SERVER,
    logging_config=dictConfig(settings.LOGGING),
)


# session = get_session("docker", "docker", "postgresql", "5432", "test_db")


@app.on_worker_init.connect()
def on_worker_init(app, **kwargs):
    logger.info(f'Working starting for app {app}')
    logger.info(f'Working starting for app {app}')


def main() -> None:
    app.main()
