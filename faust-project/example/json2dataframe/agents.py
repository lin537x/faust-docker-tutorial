# repo build
# 解析欄位
import logging
import faust
import json
import pandas as pd
import numpy as np

from example.app import app# , session
from simple_settings import settings
from example.json2dataframe.schema import rename_jcic_atoms_df_columns, BAM028, BAM095, KRM040, KRM046, STM007, VAM106, VAM107, VAM108
from example.json2dataframe.schema import Data_BCDEJKLNGHIMFA  # oracle data


logger = logging.getLogger(__name__)


upstream_topic = app.topic('SCR-SCORING-DATR-M001SHIELDREQ', value_type=bytes)  # create topic first
downstream_topic = app.topic('SCR-SCORING-DATR-M001SHIELDRPL')
jcic_atoms = ["BAM028", "BAM095", "KRM040", "KRM046", "STM007", "VAM106", "VAM107", "VAM108"]
jcic_atoms_df = [BAM028, BAM095, KRM040, KRM046, STM007, VAM106, VAM107, VAM108]


"""
Index(['MWHEADER.SOURCECHANNEL', 'MWHEADER.TXNSEQ', 'MWHEADER.O360SEQ',
       'TRANRQ.SubChannel', 'TRANRQ.JCIC', 'TRANRQ.CaseNo', 'TRANRQ.IdNo',
       'TRANRQ.Education', 'TRANRQ.MaritalStatus', 'TRANRQ.Age',
       'TRANRQ.Gender', 'TRANRQ.Industry', 'TRANRQ.Occupation',
       'TRANRQ.YearsWorking', 'TRANRQ.Income', 'TRANRQ.ResidenceYearsLmt',
       'TRANRQ.ResidenceStatus', 'TRANRQ.HasChildren', 'TRANRQ.ContactAddress',
       'TRANRQ.MobilePhone', 'TRANRQ.EmployerID', 'TRANRQ.Email',
       'TRANRQ.InsurancePolicyValue', 'TRANRQ.IsPolicyLoan', 'TRANRQ.IsCLI',
       'TRANRQ.Last1YearAvgBal', 'TRANRQ.ScoringModel'], dtype='object')
"""


@app.agent(upstream_topic, sink=[downstream_topic])
async def source2dataframe(stream):
    async for event in stream:
        try:
            """檢核欄位"""
            logger.info(f"Start... TXNSEQ={event['MWHEADER']['TXNSEQ']}, O360SEQ={event['MWHEADER']['O360SEQ']}")
            data = event["TRANRQ"]
            application_data_df = pd.DataFrame({
                'CUSTOMER_ID': [data["IdNo"]],
                'application_nbr' : [data["CaseNo"]],
                'Rating_date' : [data["JCIC"][0]['TRANRS']["SysDate"]],
                'education': [data["Education"]],
                'house_ownership': [data["ResidenceStatus"]],
                'marital': [data["MaritalStatus"]],
                'age': [data["Age"]],
                'house_living_yr': [data["ResidenceYearsLmt"]],
                'working_yr': [data["YearsWorking"]]
            })
            # logger.info(application_data_df.head())
            jcic_atoms_ = jcic_atoms.copy()
            jcic_atoms_df_dict = dict([(k, df) for k, df in zip(jcic_atoms_, jcic_atoms_df)])
            for jcic in data["JCIC"]:
                logger.info(f"Processing... {jcic['Item']}")
                for k, v in jcic["TRANRS"].items():
                    if k in jcic_atoms_ and len(v) > 0: 
                        logger.info(f"Parsing data for item {k} ...")
                        jcic_df = pd.json_normalize(v)
                        jcic_atoms_df_dict[k] = jcic_df
                        jcic_atoms_.remove(k)
                        # logger.info(jcic_df.head())

            if len(jcic_atoms_) != 0:
                logger.warning(f"Missing jcic data from source, {jcic_atoms_}")

            rename_jcic_atoms_df_columns(jcic_atoms_df_dict)

            logger.info("===================================")
            for v in jcic_atoms_df_dict.values():
                logger.info(v.head())

            # Connect Database and transform into pandas dataframe
            # with session as s:
            #     data = s.execute("select * from user_info")
            # logger.info([d for d in data])
            query_oracle_data = Data_BCDEJKLNGHIMFA

            # Business Logic ( RI )
            # BAM095
            #BAM095 Step1 共用額度調整及原始欄位處理
            # 欄位名稱&數值調整
            ## 欄位調整
            ## 千元→元
            BAM028, BAM095, KRM040, KRM046, STM007, VAM106, VAM107, VAM108 = \
                jcic_atoms_df_dict["BAM028"], jcic_atoms_df_dict["BAM095"], jcic_atoms_df_dict["KRM040"], jcic_atoms_df_dict["KRM046"], \
                jcic_atoms_df_dict["STM007"], jcic_atoms_df_dict["VAM106"], jcic_atoms_df_dict["VAM107"], jcic_atoms_df_dict["VAM108"]

            VAM = VAM106.merge(VAM107).merge(VAM108)

            BAM095=BAM095.rename(
                columns={
                        'IDN_BAN':'CUSTOMER_ID',
                        'SYSDATE':'QUERY_DATE',
                        'ACCOUNT_CODE_FLAG':'ACCOUNT_CODE2',
                        'SUM_LIMIT_AMT':'CONTRACT_AMT1',
                        'SPLIT_LIMIT_AMT':'CONTRACT_AMT',
                        'UNDUE_LOAN_AMT':'LOAN_AMT',
                        'PAY_12_CODE':'PAY_CODE_12',
                        'GUAR_KIND_CODE':'IS_KIND',
                        'CO_LOAN_FLAG':'CO_LOAN',
                        'CUR_LIMIT_CODE':'CONTRACT_CODE',
                        'TOP_LIMIT_CODE':'CONTRACT_CODE1',
                        'Y_ACCT_LIMIT_FLAG':'ACT_Y_MARK',
                        'Y_ACCT_LIMIT_AMT':'CONTRACT_AMT_Y',
                        'AC_PURE_AMT':'AC_AMT',
                        'CURRENCY_CODE':'CURRENCY_CODE_3',
                        'REVOLVING_FLAG':'CYCLE_FLAG',
                        'OFF_LIMIT_FLAG':'OFF_FLAG'
                    }
                )

            BAM095['CONTRACT_AMT1']=BAM095['CONTRACT_AMT1']*1000
            BAM095['CONTRACT_AMT']=BAM095['CONTRACT_AMT']*1000
            BAM095['LOAN_AMT']=BAM095['LOAN_AMT']*1000
            BAM095['PASS_DUE_AMT']=BAM095['PASS_DUE_AMT']*1000
            BAM095['CONTRACT_AMT_Y']=BAM095['CONTRACT_AMT_Y']*1000
            BAM095['AC_AMT']=BAM095['AC_AMT']*1000
            ##新增放款類型標籤
            BAM095['LOAN_FLAG']= np.where(BAM095['ACCOUNT_CODE']!='Y','Y','N')
            BAM095['CASHCARD_FLAG']= np.where(BAM095['ACCOUNT_CODE']=='Y','Y','N')
            BAM095['LN_CYCLE_FLAG']= np.where((BAM095['LOAN_FLAG']=='Y') & (BAM095['ACCOUNT_CODE']!='') & (BAM095['CYCLE_FLAG']=='Y'),'Y','N')
            BAM095['AMORT_FLAG']= np.where((BAM095['LOAN_FLAG']=='Y') & (BAM095['ACCOUNT_CODE']!='') & (BAM095['LN_CYCLE_FLAG']=='N'),'Y','N')
            BAM095['UNSEC_FLAG']= np.where((BAM095['LOAN_FLAG']=='Y') & (BAM095['ACCOUNT_CODE']!='') & (BAM095['IS_KIND'].isin(['','00','01','02','03','04','05','06','07','08','09'])),'Y','N')
            BAM095['UNSEC_CYCLE_FLAG']= np.where((BAM095['UNSEC_FLAG']=='Y') & (BAM095['LN_CYCLE_FLAG']=='Y') ,'Y','N')
            BAM095['UNSEC_AMORT_FLAG']= np.where((BAM095['UNSEC_FLAG']=='Y') & (BAM095['AMORT_FLAG']=='Y'),'Y','N')
            BAM095['PL_FLAG']= np.where((BAM095['ACCOUNT_CODE']=='H') & (BAM095['IS_KIND'].isin(['','00'])),'Y','N')
            BAM095['SL_FLAG']= np.where((BAM095['ACCOUNT_CODE']=='Z') & (BAM095['IS_KIND'].isin(['','00','01','02','03','04','05','06','07','08','09'])),'Y','N')
            BAM095['SEC_FLAG']= np.where((BAM095['LOAN_FLAG']=='Y') & (BAM095['ACCOUNT_CODE']!='') & (BAM095['UNSEC_FLAG']=='N'),'Y','N')
            BAM095['SEC_CYCLE_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['LN_CYCLE_FLAG']=='Y'),'Y','N')
            BAM095['SEC_AMORT_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['AMORT_FLAG']=='Y'),'Y','N')
            BAM095['SECURITY_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['IS_KIND'].str[0:1]=='1'),'Y','N')
            BAM095['MP_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['IS_KIND'].str[0:1]=='3'),'Y','N')
            BAM095['AL_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['IS_KIND'].str[0:2]=='31'),'Y','N')
            BAM095['RE_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['IS_KIND'].str[0:1]=='2'),'Y','N')
            BAM095['MTG_FLAG']= np.where((BAM095['SEC_FLAG']=='Y') & (BAM095['IS_KIND'].str[0:2]=='25'),'Y','N')
            BAM095['MTG_CYCLE_FLAG']= np.where((BAM095['MTG_FLAG']=='Y') & (BAM095['LN_CYCLE_FLAG']=='Y'),'Y','N')
            BAM095['MTG_AMORT_FLAG']= np.where((BAM095['MTG_FLAG']=='Y') & (BAM095['LN_CYCLE_FLAG']=='N'),'Y','N')
            BAM095['MTG_RE_FLAG']= np.where((BAM095['MTG_FLAG']=='Y') & (BAM095['PURPOSE_CODE']=='1'),'Y','N')
            BAM095['MTG_INVT_FLAG']= np.where((BAM095['MTG_FLAG']=='Y') & (BAM095['PURPOSE_CODE'].isin(['3','4'])),'Y','N')
            BAM095['BANK_NO']= BAM095['BANK_CODE'].str[0:3]
            
            ##非共用額度/共用額度相關欄位新增調整
            
            ##########################################################
            ##paycode替換用DICT
            # creating dictionary for trans table
            trans_dict ={'X':'0','A':'1','B':'1','1':'2','2':'3','3':'4','4':'5','5':'6','6':'7'}
            # creating translate table from dictionary
            trans_table ='XAB123456'.maketrans(trans_dict)
            ##########################################################
            BAM095['CONTRACT_AMT_N']=np.select([BAM095['CONTRACT_AMT']==0],[BAM095['CONTRACT_AMT1']], default=BAM095['CONTRACT_AMT'])
            BAM095['BANK_NO_CONTRACT_CODECNT']=BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CUSTOMER_ID'].transform('count')
            BAM095['BANK_NO_CONTRACT_CODE_MAXCONTRACT_AMT1']=BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CONTRACT_AMT1'].transform('max')
            BAM095['BANK_NO_CONTRACT_CODE_SUMCONTRACT_AMT_N']=BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CONTRACT_AMT_N'].transform('sum')
            
            
            BAM095['CONTRACT_CODE1_TYPE']=np.select([
                BAM095['CONTRACT_CODE1'].isnull(),
                BAM095['CONTRACT_CODE1']=='99999999999999999999999999999999999999999999999999',
                BAM095['BANK_NO_CONTRACT_CODECNT']==1
            ],
            [
                '00',
                '11',
                '21'
            
            ], default='22'
            )
            BAM095['MAX_CONTRACT_AMT1']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095['BANK_NO_CONTRACT_CODE_MAXCONTRACT_AMT1']])
            BAM095['SUM_CONTRACT_AMT']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095['BANK_NO_CONTRACT_CODE_SUMCONTRACT_AMT_N']])
            BAM095['CONTRACT_CODE1_TYPEAMT']=np.select([
                (BAM095['CONTRACT_CODE1_TYPE']=='22') & (BAM095['MAX_CONTRACT_AMT1']>BAM095['SUM_CONTRACT_AMT']),
                (BAM095['CONTRACT_CODE1_TYPE']=='22') & (BAM095['MAX_CONTRACT_AMT1']==BAM095['SUM_CONTRACT_AMT']),
                (BAM095['CONTRACT_CODE1_TYPE']=='22') & (BAM095['MAX_CONTRACT_AMT1']<BAM095['SUM_CONTRACT_AMT'])
            ],
            [
                '0',
                '1',
                '2'
            ]
            )
            
            ##無共用額度的部份
            BAM095_nonshare=BAM095.loc[(BAM095['CONTRACT_CODE1_TYPE']!='22') | (BAM095['CONTRACT_CODE1_TYPEAMT']!='2') | BAM095['CONTRACT_CODE1_TYPEAMT'].isnull()][
                ['CUSTOMER_ID','APPLICATION_NBR','DATA_YYY','DATA_MM','BANK_NO','CONTRACT_CODE1','LOAN_AMT','PASS_DUE_AMT'
                ,'ACCOUNT_CODE','ACCOUNT_CODE2','LOAN_FLAG','CASHCARD_FLAG','LN_CYCLE_FLAG','AMORT_FLAG','UNSEC_FLAG'
                ,'UNSEC_CYCLE_FLAG','UNSEC_AMORT_FLAG','PL_FLAG','SL_FLAG','SEC_FLAG','SEC_CYCLE_FLAG','SEC_AMORT_FLAG'
                ,'SECURITY_FLAG','MP_FLAG','AL_FLAG','RE_FLAG','MTG_FLAG','MTG_CYCLE_FLAG','MTG_AMORT_FLAG','MTG_RE_FLAG'
                ,'MTG_INVT_FLAG','PAY_CODE_12','CONTRACT_AMT_Y','CONTRACT_AMT1','CONTRACT_AMT_N','QUERY_DATE']
            ]
            BAM095_nonshare['TOTAL_AMT']=BAM095_nonshare['LOAN_AMT']+BAM095_nonshare['PASS_DUE_AMT']
            BAM095_nonshare['CONTRACT_AMT1']=np.where((BAM095_nonshare['CASHCARD_FLAG']=='Y'),BAM095_nonshare['CONTRACT_AMT_Y'],BAM095_nonshare['CONTRACT_AMT1'])
            BAM095_nonshare['CONTRACT_AMT']=np.where((BAM095_nonshare['CASHCARD_FLAG']=='Y'),BAM095_nonshare['CONTRACT_AMT_Y'],BAM095_nonshare['CONTRACT_AMT_N'])
            BAM095_nonshare['PAY_CODE_12_ADJ_DELQ']=BAM095_nonshare['PAY_CODE_12'].str.translate(trans_table)
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_1ST']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[0]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_2ND']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[1]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_3RD']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[2]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_4TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[3]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_5TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[4]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_6TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[5]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_7TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[6]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_8TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[7]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_9TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[8]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_10TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[9]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_11TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[10]
            BAM095_nonshare['PAY_CODE_ADJ_DELQ_12TH']=BAM095_nonshare['PAY_CODE_12_ADJ_DELQ'].str[11]
            
            
            ##有共用額度的部份
            
            #擔保品類別 1房產 2不動產 3動產 4債券 5貴重金屬 6信保 7其他
            BAM095['KIND_FLG']=np.select([
                BAM095['IS_KIND'].str.strip()=='25',
                BAM095['IS_KIND'].str.strip().str[0]=='2',
                BAM095['IS_KIND'].str.strip().str[0]=='3',
                BAM095['IS_KIND'].str.strip().str[0]=='1',
                BAM095['IS_KIND'].str.strip().str[0]=='4',
                (BAM095['IS_KIND'].str.strip().str[0]=='0') & (BAM095['IS_KIND'].str.strip()!='00'),
            ],
            [
                '1','2','3','4','5','6'
            ], default='7'
            )
            BAM095['PAY_LEN']=BAM095['PAY_CODE_12'].str.strip().str.len()
            BAM095['PAY_LEN_MAX']=BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_LEN'].transform('max')
            BAM095['ORIGIN_LOAN_AMT']=BAM095['LOAN_AMT']
            BAM095['LOAN_AMT']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['LOAN_AMT'].transform('sum')], default=BAM095['ORIGIN_LOAN_AMT'])
            BAM095['PASS_DUE_AMT']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PASS_DUE_AMT'].transform('sum')], default=BAM095['PASS_DUE_AMT'])
            BAM095['CONTRACT_AMT1']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CONTRACT_AMT1'].transform('max')], default=BAM095['CONTRACT_AMT1'])
            BAM095['CONTRACT_AMT']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CONTRACT_AMT1'].transform('max')], default=BAM095['CONTRACT_AMT1'])
            BAM095['CONTRACT_AMT_Y']=np.select([BAM095['CONTRACT_CODE1_TYPE']=='22'],[BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['CONTRACT_AMT_Y'].transform('max')], default=BAM095['CONTRACT_AMT_Y'])
            BAM095['PAY_CODE_12_ADJ_DELQ_by_ln']=BAM095['PAY_CODE_12'].str.translate(trans_table)
            BAM095['PAY_CODE1']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[0].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE2']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[1].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE3']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[2].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE4']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[3].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE5']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[4].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE6']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[5].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE7']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[6].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE8']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[7].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE9']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[8].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE10']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[9].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE11']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[10].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE12']=BAM095['PAY_CODE_12_ADJ_DELQ_by_ln'].str[11].astype(str).replace(' ',None).astype(int)
            BAM095['PAY_CODE_12_ADJ_DELQ1']=BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE1'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE2'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE3'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE4'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE5'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE6'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE7'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE8'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE9'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE10'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE11'].transform('max').astype(str)+BAM095.groupby(['BANK_NO','CONTRACT_CODE1'])['PAY_CODE12'].transform('max').astype(str)
            BAM095['PAY_CODE_12_ADJ_DELQ']=BAM095.apply(lambda BAM095: BAM095['PAY_CODE_12_ADJ_DELQ1'][0:BAM095['PAY_LEN_MAX']], axis=1)
            ##共用額度Qualigy rank取一筆
            BAM095['QUALIFY_RK']=BAM095.sort_values(['KIND_FLG','PAY_LEN_MAX','ORIGIN_LOAN_AMT'], ascending=[True,False,False]).groupby(['BANK_NO','CONTRACT_CODE1','CONTRACT_CODE1_TYPE','CONTRACT_CODE1_TYPEAMT']).cumcount() + 1
            BAM095_share=BAM095.loc[(BAM095['CONTRACT_CODE1_TYPE']=='22') & (BAM095['CONTRACT_CODE1_TYPEAMT']=='2') & (BAM095['QUALIFY_RK']=='1')][[
                'CUSTOMER_ID','APPLICATION_NBR','DATA_YYY','DATA_MM','BANK_NO','CONTRACT_CODE1','LOAN_AMT','PASS_DUE_AMT'
                ,'ACCOUNT_CODE','ACCOUNT_CODE2','LOAN_FLAG','CASHCARD_FLAG','LN_CYCLE_FLAG','AMORT_FLAG','UNSEC_FLAG'
                ,'UNSEC_CYCLE_FLAG','UNSEC_AMORT_FLAG','PL_FLAG','SL_FLAG','SEC_FLAG','SEC_CYCLE_FLAG','SEC_AMORT_FLAG'
                ,'SECURITY_FLAG','MP_FLAG','AL_FLAG','RE_FLAG','MTG_FLAG','MTG_CYCLE_FLAG','MTG_AMORT_FLAG','MTG_RE_FLAG'
                ,'MTG_INVT_FLAG','PAY_CODE_12','CONTRACT_AMT_Y','CONTRACT_AMT','CONTRACT_AMT1','CONTRACT_AMT_N','QUERY_DATE'
                ,'PAY_CODE_12_ADJ_DELQ']
            ]
            BAM095_share['TOTAL_AMT']=BAM095_share['LOAN_AMT']+BAM095_share['PASS_DUE_AMT']
            BAM095_share['CONTRACT_AMT1']=np.where((BAM095_share['CASHCARD_FLAG']=='Y'),BAM095_share['CONTRACT_AMT_Y'],BAM095_share['CONTRACT_AMT1'])
            BAM095_share['CONTRACT_AMT']=np.where((BAM095_share['CASHCARD_FLAG']=='Y'),BAM095_share['CONTRACT_AMT_Y'],BAM095_share['CONTRACT_AMT'])
            BAM095_share['PAY_CODE_ADJ_DELQ_1ST']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[0]
            BAM095_share['PAY_CODE_ADJ_DELQ_2ND']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[1]
            BAM095_share['PAY_CODE_ADJ_DELQ_3RD']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[2]
            BAM095_share['PAY_CODE_ADJ_DELQ_4TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[3]
            BAM095_share['PAY_CODE_ADJ_DELQ_5TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[4]
            BAM095_share['PAY_CODE_ADJ_DELQ_6TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[5]
            BAM095_share['PAY_CODE_ADJ_DELQ_7TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[6]
            BAM095_share['PAY_CODE_ADJ_DELQ_8TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[7]
            BAM095_share['PAY_CODE_ADJ_DELQ_9TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[8]
            BAM095_share['PAY_CODE_ADJ_DELQ_10TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[9]
            BAM095_share['PAY_CODE_ADJ_DELQ_11TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[10]
            BAM095_share['PAY_CODE_ADJ_DELQ_12TH']=BAM095_share['PAY_CODE_12_ADJ_DELQ'].str[11]
            
            ## CONCAT有共用和無共用的部份
            BAM095_adj=pd.concat([BAM095_nonshare[[
                    'CUSTOMER_ID','APPLICATION_NBR','DATA_YYY','DATA_MM','BANK_NO','CONTRACT_CODE1','LOAN_AMT','PASS_DUE_AMT'
                    ,'TOTAL_AMT','CONTRACT_AMT1','CONTRACT_AMT','ACCOUNT_CODE','ACCOUNT_CODE2','LOAN_FLAG','CASHCARD_FLAG'
                    ,'LN_CYCLE_FLAG','AMORT_FLAG','UNSEC_FLAG','UNSEC_CYCLE_FLAG','UNSEC_AMORT_FLAG','PL_FLAG','SL_FLAG'
                    ,'SEC_FLAG','SEC_CYCLE_FLAG','SEC_AMORT_FLAG','SECURITY_FLAG','MP_FLAG','AL_FLAG','RE_FLAG','MTG_FLAG'
                    ,'MTG_CYCLE_FLAG','MTG_AMORT_FLAG','MTG_RE_FLAG','MTG_INVT_FLAG','PAY_CODE_12','PAY_CODE_12_ADJ_DELQ'
                    ,'PAY_CODE_ADJ_DELQ_1ST','PAY_CODE_ADJ_DELQ_2ND','PAY_CODE_ADJ_DELQ_3RD','PAY_CODE_ADJ_DELQ_4TH'
                    ,'PAY_CODE_ADJ_DELQ_5TH','PAY_CODE_ADJ_DELQ_6TH','PAY_CODE_ADJ_DELQ_7TH','PAY_CODE_ADJ_DELQ_8TH'
                    ,'PAY_CODE_ADJ_DELQ_9TH','PAY_CODE_ADJ_DELQ_10TH','PAY_CODE_ADJ_DELQ_11TH','PAY_CODE_ADJ_DELQ_12TH','QUERY_DATE']
                ],
                BAM095_share[[
                    'CUSTOMER_ID','APPLICATION_NBR','DATA_YYY','DATA_MM','BANK_NO','CONTRACT_CODE1','LOAN_AMT','PASS_DUE_AMT'
                    ,'TOTAL_AMT','CONTRACT_AMT1','CONTRACT_AMT','ACCOUNT_CODE','ACCOUNT_CODE2','LOAN_FLAG','CASHCARD_FLAG'
                    ,'LN_CYCLE_FLAG','AMORT_FLAG','UNSEC_FLAG','UNSEC_CYCLE_FLAG','UNSEC_AMORT_FLAG','PL_FLAG','SL_FLAG'
                    ,'SEC_FLAG','SEC_CYCLE_FLAG','SEC_AMORT_FLAG','SECURITY_FLAG','MP_FLAG','AL_FLAG','RE_FLAG','MTG_FLAG'
                    ,'MTG_CYCLE_FLAG','MTG_AMORT_FLAG','MTG_RE_FLAG','MTG_INVT_FLAG','PAY_CODE_12','PAY_CODE_12_ADJ_DELQ'
                    ,'PAY_CODE_ADJ_DELQ_1ST','PAY_CODE_ADJ_DELQ_2ND','PAY_CODE_ADJ_DELQ_3RD','PAY_CODE_ADJ_DELQ_4TH'
                    ,'PAY_CODE_ADJ_DELQ_5TH','PAY_CODE_ADJ_DELQ_6TH','PAY_CODE_ADJ_DELQ_7TH','PAY_CODE_ADJ_DELQ_8TH'
                    ,'PAY_CODE_ADJ_DELQ_9TH','PAY_CODE_ADJ_DELQ_10TH','PAY_CODE_ADJ_DELQ_11TH','PAY_CODE_ADJ_DELQ_12TH','QUERY_DATE']
                ]
            ])
            
            #BAM095 Step2 變數生成
            #paycode by id
            BAM095_VAR=BAM095_adj.groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_1ST':'max','PAY_CODE_ADJ_DELQ_2ND':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_3RD':'max','PAY_CODE_ADJ_DELQ_4TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_5TH':'max','PAY_CODE_ADJ_DELQ_6TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_7TH':'max','PAY_CODE_ADJ_DELQ_8TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_9TH':'max','PAY_CODE_ADJ_DELQ_10TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_11TH':'max','PAY_CODE_ADJ_DELQ_12TH':'max'
                                                  })
            BAM095_VAR['PAY_CODE12']=BAM095_VAR['PAY_CODE_ADJ_DELQ_1ST']+BAM095_VAR['PAY_CODE_ADJ_DELQ_2ND']+BAM095_VAR['PAY_CODE_ADJ_DELQ_3RD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_4TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_5TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_6TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_7TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_8TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_9TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_10TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_11TH']+BAM095_VAR['PAY_CODE_ADJ_DELQ_12TH']
            BAM095_adj['PAY_CODE_12_ADJ_DELQ_TRIM']=BAM095_adj['PAY_CODE_12_ADJ_DELQ'].str.strip()
            BAM095_VAR['PAY_CODE_ADJ_SEQ_B']=BAM095_VAR['PAY_CODE12'].str[:max(BAM095_adj['PAY_CODE_12_ADJ_DELQ_TRIM'].str.len())]
            #現金卡
            BAM095_VAR['PAY_CODE_ADJ_DELQ_1ST_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_1ST':'max'})['PAY_CODE_ADJ_DELQ_1ST']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_2ND_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_2ND':'max'})['PAY_CODE_ADJ_DELQ_2ND']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_3RD_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_3RD':'max'})['PAY_CODE_ADJ_DELQ_3RD']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_4TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_4TH':'max'})['PAY_CODE_ADJ_DELQ_4TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_5TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_5TH':'max'})['PAY_CODE_ADJ_DELQ_5TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_6TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_6TH':'max'})['PAY_CODE_ADJ_DELQ_6TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_7TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_7TH':'max'})['PAY_CODE_ADJ_DELQ_7TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_8TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_8TH':'max'})['PAY_CODE_ADJ_DELQ_8TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_9TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_9TH':'max'})['PAY_CODE_ADJ_DELQ_9TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_10TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_10TH':'max'})['PAY_CODE_ADJ_DELQ_10TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_11TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_11TH':'max'})['PAY_CODE_ADJ_DELQ_11TH']
            BAM095_VAR['PAY_CODE_ADJ_DELQ_12TH_CASHCARD']=BAM095_adj[BAM095_adj['CASHCARD_FLAG']=='Y'].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_12TH':'max'})['PAY_CODE_ADJ_DELQ_12TH']
            BAM095_VAR['CASHCARD_PAY_CODE_ADJ_DELQ']=BAM095_VAR['PAY_CODE_ADJ_DELQ_1ST_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_2ND_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_3RD_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_4TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_5TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_6TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_7TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_8TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_9TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_10TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_11TH_CASHCARD']+BAM095_VAR['PAY_CODE_ADJ_DELQ_12TH_CASHCARD']
            ##近N月最差繳款狀態
            BAM095_VAR['JLN2_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')]['PAY_CODE_ADJ_DELQ_1ST'].agg('max')]
            BAM095_adj['M3PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST','PAY_CODE_ADJ_DELQ_2ND','PAY_CODE_ADJ_DELQ_3RD']].max(axis=1)
            BAM095_VAR['JLN4_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')]['M3PAYCODE'].agg('max').astype(int)]
            BAM095_adj['M6PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST','PAY_CODE_ADJ_DELQ_2ND','PAY_CODE_ADJ_DELQ_3RD','PAY_CODE_ADJ_DELQ_4TH','PAY_CODE_ADJ_DELQ_5TH','PAY_CODE_ADJ_DELQ_6TH']].max(axis=1)
            BAM095_VAR['JLN5_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')]['M6PAYCODE'].agg('max').astype(int)]
            BAM095_adj['M12PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST','PAY_CODE_ADJ_DELQ_2ND','PAY_CODE_ADJ_DELQ_3RD','PAY_CODE_ADJ_DELQ_4TH','PAY_CODE_ADJ_DELQ_5TH','PAY_CODE_ADJ_DELQ_6TH','PAY_CODE_ADJ_DELQ_7TH','PAY_CODE_ADJ_DELQ_8TH','PAY_CODE_ADJ_DELQ_9TH','PAY_CODE_ADJ_DELQ_10TH','PAY_CODE_ADJ_DELQ_11TH','PAY_CODE_ADJ_DELQ_12TH']].max(axis=1)
            BAM095_VAR['JLN6_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')]['M12PAYCODE'].agg('max').astype(int)]
            ##近N月M1+帳戶數_授信
            BAM095_VAR['JLN7_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN7_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN7_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN8_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0))]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN8_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0))]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN8_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='') & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0) | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0))]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN9_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')
                                                 & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')
                                                 & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int)>0)
                                                   )]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN9_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')
                                                 & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')
                                                 & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int)>0)
                                                   )]['CUSTOMER_ID'].agg('count')]
            BAM095_VAR['JLN9_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')
                                                 & (BAM095_adj['PAY_CODE_12_ADJ_DELQ']!='')
                                                 & ((BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int)>0)
                                                    | (BAM095_adj['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int)>0)
                                                   )]['CUSTOMER_ID'].agg('count')]
            ##近N月M1+月數_授信
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_1']=1
                BAM095_VAR['JLN11_1']=1
                BAM095_VAR['JLN12_1']=1
            else:
                BAM095_VAR['JLN10_1']=0
                BAM095_VAR['JLN11_1']=0
                BAM095_VAR['JLN12_1']=0
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_1']=BAM095_VAR['JLN10_1']+1
                BAM095_VAR['JLN11_1']=BAM095_VAR['JLN11_1']+1
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_1']=BAM095_VAR['JLN10_1']+1
                BAM095_VAR['JLN11_1']=BAM095_VAR['JLN11_1']+1
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_1']=BAM095_VAR['JLN11_1']+1
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_1']=BAM095_VAR['JLN11_1']+1
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_1']=BAM095_VAR['JLN11_1']+1
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_1']=BAM095_VAR['JLN12_1']+1
            
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_4']=1
                BAM095_VAR['JLN11_4']=1
                BAM095_VAR['JLN12_4']=1
            else:
                BAM095_VAR['JLN10_4']=0
                BAM095_VAR['JLN11_4']=0
                BAM095_VAR['JLN12_4']=0
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_4']=BAM095_VAR['JLN10_4']+1
                BAM095_VAR['JLN11_4']=BAM095_VAR['JLN11_4']+1
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_4']=BAM095_VAR['JLN10_4']+1
                BAM095_VAR['JLN11_4']=BAM095_VAR['JLN11_4']+1
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_4']=BAM095_VAR['JLN11_4']+1
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_4']=BAM095_VAR['JLN11_4']+1
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_4']=BAM095_VAR['JLN11_4']+1
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_4']=BAM095_VAR['JLN12_4']+1
            
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_9']=1
                BAM095_VAR['JLN11_9']=1
                BAM095_VAR['JLN12_9']=1
            else:
                BAM095_VAR['JLN10_9']=0
                BAM095_VAR['JLN11_9']=0
                BAM095_VAR['JLN12_9']=0
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_9']=BAM095_VAR['JLN10_9']+1
                BAM095_VAR['JLN11_9']=BAM095_VAR['JLN11_9']+1
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN10_9']=BAM095_VAR['JLN10_9']+1
                BAM095_VAR['JLN11_9']=BAM095_VAR['JLN11_9']+1
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_9']=BAM095_VAR['JLN11_9']+1
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_9']=BAM095_VAR['JLN11_9']+1
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN11_9']=BAM095_VAR['JLN11_9']+1
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['JLN12_9']=BAM095_VAR['JLN12_9']+1
            ##近一次M1+距今月數
            ##不確定即時JCIC查詢PAYCODE有沒有空格 所以用最笨的方法寫
            BAM095_VAR['JLN13_1']=0
            BAM095_VAR['JLN13_4']=0
            BAM095_VAR['JLN13_9']=0
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=1
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=1
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=1
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=2
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=2
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=2
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=3
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=3
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=3
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=4
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=4
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=4
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=5
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=5
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=5
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=6
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=6
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=6
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=7
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=7
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=7
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=8
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=8
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=8
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=9
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=9
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=9
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=10
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=10
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=10
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=11
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=11
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=11
            if BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_1']=12
            if BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_4']=12
            if BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['JLN13_9']=12
            ##當月M1+餘額總和
            BAM095_VAR['JLN3_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN3_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN3_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['TOTAL_AMT'].agg('sum')]
            ##餘額總和
            BAM095_VAR['JLN14_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_5']=[BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_6']=[BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_7']=[BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_8']=[BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_10']=[BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_11']=[BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_14']=[BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_19']=[BAM095_adj.loc[(BAM095_adj['MTG_RE_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['JLN14_20']=[BAM095_adj.loc[(BAM095_adj['MTG_INVT_FLAG']=='Y')]['TOTAL_AMT'].agg('sum')]
            ##餘額佔比
            BAM095_VAR['JLN15_2']=np.where(BAM095_VAR['JLN14_1']>0,BAM095_VAR['JLN14_2']/BAM095_VAR['JLN14_1'],0)
            BAM095_VAR['JLN15_3']=np.where(BAM095_VAR['JLN14_1']>0,BAM095_VAR['JLN14_3']/BAM095_VAR['JLN14_1'],0)
            BAM095_VAR['JLN15_4']=np.where(BAM095_VAR['JLN14_1']>0,BAM095_VAR['JLN14_4']/BAM095_VAR['JLN14_1'],0)
            BAM095_VAR['JLN15_5']=np.where(BAM095_VAR['JLN14_4']>0,BAM095_VAR['JLN14_5']/BAM095_VAR['JLN14_4'],0)
            BAM095_VAR['JLN15_6']=np.where(BAM095_VAR['JLN14_4']>0,BAM095_VAR['JLN14_6']/BAM095_VAR['JLN14_4'],0)
            BAM095_VAR['JLN15_9']=np.where(BAM095_VAR['JLN14_1']>0,BAM095_VAR['JLN14_9']/BAM095_VAR['JLN14_1'],0)
            BAM095_VAR['JLN15_10']=np.where(BAM095_VAR['JLN14_9']>0,BAM095_VAR['JLN14_10']/BAM095_VAR['JLN14_9'],0)
            BAM095_VAR['JLN15_11']=np.where(BAM095_VAR['JLN14_9']>0,BAM095_VAR['JLN14_11']/BAM095_VAR['JLN14_9'],0)
            BAM095_VAR['JLN15_12']=np.where(BAM095_VAR['JLN14_9']>0,BAM095_VAR['JLN14_12']/BAM095_VAR['JLN14_9'],0)
            BAM095_VAR['JLN15_13']=np.where(BAM095_VAR['JLN14_9']>0,BAM095_VAR['JLN14_13']/BAM095_VAR['JLN14_9'],0)
            BAM095_VAR['JLN15_15']=np.where(BAM095_VAR['JLN14_9']>0,BAM095_VAR['JLN14_15']/BAM095_VAR['JLN14_9'],0)
            BAM095_VAR['JLN15_17']=np.where(BAM095_VAR['JLN14_16']>0,BAM095_VAR['JLN14_17']/BAM095_VAR['JLN14_16'],0)
            BAM095_VAR['JLN15_18']=np.where(BAM095_VAR['JLN14_16']>0,BAM095_VAR['JLN14_18']/BAM095_VAR['JLN14_16'],0)
            BAM095_VAR['JLN15_19']=np.where(BAM095_VAR['JLN14_16']>0,BAM095_VAR['JLN14_19']/BAM095_VAR['JLN14_16'],0)
            BAM095_VAR['JLN15_20']=np.where(BAM095_VAR['JLN14_16']>0,BAM095_VAR['JLN14_20']/BAM095_VAR['JLN14_16'],0)
            ##額度總和
            BAM095_VAR['JLN16_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_5']=[BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_6']=[BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_7']=[BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_8']=[BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_10']=[BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_11']=[BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_14']=[BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_19']=[BAM095_adj.loc[(BAM095_adj['MTG_RE_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN16_20']=[BAM095_adj.loc[(BAM095_adj['MTG_INVT_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            ##額度最大值
            BAM095_VAR['JLN17_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_19']=[BAM095_adj.loc[(BAM095_adj['MTG_RE_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['JLN17_20']=[BAM095_adj.loc[(BAM095_adj['MTG_INVT_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            ##額度總使用率
            BAM095_VAR['JLN18_1']=np.select([BAM095_VAR['JLN16_1']!=0,BAM095_VAR['JLN14_1']==0,BAM095_VAR['JLN14_1']>0],[BAM095_VAR['JLN14_1']/BAM095_VAR['JLN16_1'],0,9999],default=-9999)
            BAM095_VAR['JLN18_2']=np.select([BAM095_VAR['JLN16_2']!=0,BAM095_VAR['JLN14_2']==0,BAM095_VAR['JLN14_2']>0],[BAM095_VAR['JLN14_2']/BAM095_VAR['JLN16_2'],0,9999],default=-9999)
            BAM095_VAR['JLN18_3']=np.select([BAM095_VAR['JLN16_3']!=0,BAM095_VAR['JLN14_3']==0,BAM095_VAR['JLN14_3']>0],[BAM095_VAR['JLN14_3']/BAM095_VAR['JLN16_3'],0,9999],default=-9999)
            BAM095_VAR['JLN18_4']=np.select([BAM095_VAR['JLN16_4']!=0,BAM095_VAR['JLN14_4']==0,BAM095_VAR['JLN14_4']>0],[BAM095_VAR['JLN14_4']/BAM095_VAR['JLN16_4'],0,9999],default=-9999)
            BAM095_VAR['JLN18_5']=np.select([BAM095_VAR['JLN16_5']!=0,BAM095_VAR['JLN14_5']==0,BAM095_VAR['JLN14_5']>0],[BAM095_VAR['JLN14_5']/BAM095_VAR['JLN16_5'],0,9999],default=-9999)
            BAM095_VAR['JLN18_6']=np.select([BAM095_VAR['JLN16_6']!=0,BAM095_VAR['JLN14_6']==0,BAM095_VAR['JLN14_6']>0],[BAM095_VAR['JLN14_6']/BAM095_VAR['JLN16_6'],0,9999],default=-9999)
            BAM095_VAR['JLN18_9']=np.select([BAM095_VAR['JLN16_9']!=0,BAM095_VAR['JLN14_9']==0,BAM095_VAR['JLN14_9']>0],[BAM095_VAR['JLN14_9']/BAM095_VAR['JLN16_9'],0,9999],default=-9999)
            BAM095_VAR['JLN18_10']=np.select([BAM095_VAR['JLN16_10']!=0,BAM095_VAR['JLN14_10']==0,BAM095_VAR['JLN14_10']>0],[BAM095_VAR['JLN14_10']/BAM095_VAR['JLN16_10'],0,9999],default=-9999)
            BAM095_VAR['JLN18_11']=np.select([BAM095_VAR['JLN16_11']!=0,BAM095_VAR['JLN14_11']==0,BAM095_VAR['JLN14_11']>0],[BAM095_VAR['JLN14_11']/BAM095_VAR['JLN16_11'],0,9999],default=-9999)
            BAM095_VAR['JLN18_12']=np.select([BAM095_VAR['JLN16_12']!=0,BAM095_VAR['JLN14_12']==0,BAM095_VAR['JLN14_12']>0],[BAM095_VAR['JLN14_12']/BAM095_VAR['JLN16_12'],0,9999],default=-9999)
            BAM095_VAR['JLN18_13']=np.select([BAM095_VAR['JLN16_13']!=0,BAM095_VAR['JLN14_13']==0,BAM095_VAR['JLN14_13']>0],[BAM095_VAR['JLN14_13']/BAM095_VAR['JLN16_13'],0,9999],default=-9999)
            BAM095_VAR['JLN18_15']=np.select([BAM095_VAR['JLN16_15']!=0,BAM095_VAR['JLN14_15']==0,BAM095_VAR['JLN14_15']>0],[BAM095_VAR['JLN14_15']/BAM095_VAR['JLN16_15'],0,9999],default=-9999)
            ##額度總和(餘額>0)
            BAM095_VAR['JLN19_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_5']=[BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_6']=[BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_7']=[BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_8']=[BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_10']=[BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_11']=[BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_14']=[BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN19_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            ##帳戶數
            BAM095_VAR['JLN22_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_5']=[BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_6']=[BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_7']=[BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_8']=[BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_10']=[BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_11']=[BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_14']=[BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y')]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN22_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN22_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            ##帳戶數(餘額>0)
            BAM095_VAR['JLN23_1']=[BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_2']=[BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_3']=[BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_4']=[BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_5']=[BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_6']=[BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_7']=[BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_8']=[BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_9']=[BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_10']=[BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_11']=[BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_12']=[BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_13']=[BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_14']=[BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_15']=[BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_16']=[BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('sum')]
            BAM095_VAR['JLN23_17']=[BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['JLN23_18']=[BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)]['CONTRACT_AMT'].agg('count')]
            
            
            ##現金卡、雙卡變數用
            ##現金卡繳型序列
            BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_1ST':'max','PAY_CODE_ADJ_DELQ_2ND':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_3RD':'max','PAY_CODE_ADJ_DELQ_4TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_5TH':'max','PAY_CODE_ADJ_DELQ_6TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_7TH':'max','PAY_CODE_ADJ_DELQ_8TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_9TH':'max','PAY_CODE_ADJ_DELQ_10TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_11TH':'max','PAY_CODE_ADJ_DELQ_12TH':'max'
                                                  })
            cashcard=BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')].groupby('CUSTOMER_ID', as_index=False).agg({'PAY_CODE_ADJ_DELQ_1ST':'max','PAY_CODE_ADJ_DELQ_2ND':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_3RD':'max','PAY_CODE_ADJ_DELQ_4TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_5TH':'max','PAY_CODE_ADJ_DELQ_6TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_7TH':'max','PAY_CODE_ADJ_DELQ_8TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_9TH':'max','PAY_CODE_ADJ_DELQ_10TH':'max'
                                                  ,'PAY_CODE_ADJ_DELQ_11TH':'max','PAY_CODE_ADJ_DELQ_12TH':'max'
                                                  })
            cashcard['CASHCARD_PAY_CODE_ADJ_DELQ']=cashcard['PAY_CODE_ADJ_DELQ_1ST']+cashcard['PAY_CODE_ADJ_DELQ_2ND']+cashcard['PAY_CODE_ADJ_DELQ_3RD']+cashcard['PAY_CODE_ADJ_DELQ_4TH']+cashcard['PAY_CODE_ADJ_DELQ_5TH']+cashcard['PAY_CODE_ADJ_DELQ_6TH']+cashcard['PAY_CODE_ADJ_DELQ_7TH']+cashcard['PAY_CODE_ADJ_DELQ_8TH']+cashcard['PAY_CODE_ADJ_DELQ_9TH']+cashcard['PAY_CODE_ADJ_DELQ_10TH']+cashcard['PAY_CODE_ADJ_DELQ_11TH']+cashcard['PAY_CODE_ADJ_DELQ_12TH']
            BAM095_adj=pd.merge(BAM095_adj,cashcard,how='left',on='CUSTOMER_ID',suffixes=('','_CASHCARD'))
            ##現金卡張數
            BAM095_VAR['CASHCARD_CNT']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['CONTRACT_AMT'].agg('count')]
            ##JCC4 當月/近n月最差繳款狀態 現金卡
            BAM095_VAR['CASHCARD_JCC4_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST_CASHCARD'].agg('max')]
            BAM095_adj['M3PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST_CASHCARD','PAY_CODE_ADJ_DELQ_2ND_CASHCARD','PAY_CODE_ADJ_DELQ_3RD_CASHCARD']].max(axis=1)
            BAM095_VAR['CASHCARD_JCC4_3']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['M3PAYCODE'].agg('max')]
            BAM095_adj['M6PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST_CASHCARD','PAY_CODE_ADJ_DELQ_2ND_CASHCARD','PAY_CODE_ADJ_DELQ_3RD_CASHCARD','PAY_CODE_ADJ_DELQ_4TH_CASHCARD','PAY_CODE_ADJ_DELQ_5TH_CASHCARD','PAY_CODE_ADJ_DELQ_6TH_CASHCARD']].max(axis=1)
            BAM095_VAR['CASHCARD_JCC4_6']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['M6PAYCODE'].agg('max')]
            BAM095_adj['M12PAYCODE']=BAM095_adj[['PAY_CODE_ADJ_DELQ_1ST_CASHCARD','PAY_CODE_ADJ_DELQ_2ND_CASHCARD','PAY_CODE_ADJ_DELQ_3RD_CASHCARD','PAY_CODE_ADJ_DELQ_4TH_CASHCARD','PAY_CODE_ADJ_DELQ_5TH_CASHCARD','PAY_CODE_ADJ_DELQ_6TH_CASHCARD','PAY_CODE_ADJ_DELQ_7TH_CASHCARD','PAY_CODE_ADJ_DELQ_8TH_CASHCARD','PAY_CODE_ADJ_DELQ_9TH_CASHCARD','PAY_CODE_ADJ_DELQ_10TH_CASHCARD','PAY_CODE_ADJ_DELQ_11TH_CASHCARD','PAY_CODE_ADJ_DELQ_12TH_CASHCARD']].max(axis=1)
            BAM095_VAR['CASHCARD_JCC4_12']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['M12PAYCODE'].agg('max')]
            ##近N月M1+帳戶數_現金卡
            BAM095_VAR['CASHCARD_JCC5_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')|(BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].isin([1,2,3,4,5,6,7]))]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC5_3']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')|(BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].isin([1,2,3,4,5,6,7]))]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC5_6']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')|(BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].isin([1,2,3,4,5,6,7]))]['CONTRACT_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC5_12']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')|(BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_2ND'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_3RD'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_4TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_5TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_6TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_7TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_8TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_9TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_10TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_11TH'].isin([1,2,3,4,5,6,7]))|(BAM095_adj['PAY_CODE_ADJ_DELQ_12TH'].isin([1,2,3,4,5,6,7]))]['CONTRACT_AMT'].agg('count')]
            ##近N月M1+月數_授信
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_1']=1
                BAM095_VAR['CASHCARD_JCC6_3']=1
                BAM095_VAR['CASHCARD_JCC6_6']=1
                BAM095_VAR['CASHCARD_JCC6_12']=1
            else:
                BAM095_VAR['CASHCARD_JCC6_1']=0
                BAM095_VAR['CASHCARD_JCC6_3']=0
                BAM095_VAR['CASHCARD_JCC6_6']=0
                BAM095_VAR['CASHCARD_JCC6_12']=0
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_3']=BAM095_VAR['CASHCARD_JCC6_3']+1
                BAM095_VAR['CASHCARD_JCC6_6']=BAM095_VAR['CASHCARD_JCC6_6']+1
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_3']=BAM095_VAR['CASHCARD_JCC6_3']+1
                BAM095_VAR['CASHCARD_JCC6_6']=BAM095_VAR['CASHCARD_JCC6_6']+1
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_6']=BAM095_VAR['CASHCARD_JCC6_6']+1
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_6']=BAM095_VAR['CASHCARD_JCC6_6']+1
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_6']=BAM095_VAR['CASHCARD_JCC6_6']+1
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max')>0:
                BAM095_VAR['CASHCARD_JCC6_12']=BAM095_VAR['CASHCARD_JCC6_12']+1
            ##近一次M1+距今月數
            ##不確定即時JCIC查詢PAYCODE有沒有空格 所以用最笨的方法寫
            BAM095_VAR['CASHCARD_JCC7_12']=0
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=1
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_2ND'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=2
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_3RD'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=3
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_4TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=4
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_5TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=5
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_6TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=6
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_7TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=7
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_8TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=8
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_9TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=9
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_10TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=10
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_11TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=11
            if BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['PAY_CODE_ADJ_DELQ_12TH'].replace(' ',0).astype(int).agg('max') in [1,2,3,4,5,6]:
                BAM095_VAR['CASHCARD_JCC7_12']=12
            
            BAM095_VAR['CASHCARD_JCC8_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') & (BAM095_adj['PAY_CODE_ADJ_DELQ_1ST'].replace(' ',0).astype(int)>0)]['TOTAL_AMT'].agg('sum')]
            BAM095_VAR['CASHCARD_JCC9_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['CONTRACT_AMT1'].agg('sum')]
            BAM095_VAR['CASHCARD_JCC10_1']=np.where(BAM095_VAR['CASHCARD_CNT']==0,0,BAM095_VAR['CASHCARD_JCC9_1']/BAM095_VAR['CASHCARD_CNT'])
            BAM095_VAR['CASHCARD_JCC11_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['CONTRACT_AMT1'].agg('max')]
            BAM095_VAR['CASHCARD_JCC12_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')]['CONTRACT_AMT1'].agg('min')]
            BAM095_VAR['CASHCARD_JCC13_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') & (BAM095_adj['CONTRACT_AMT1']<=50000)]['TOTAL_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC14_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') & (BAM095_adj['CONTRACT_AMT1']<=300000)]['TOTAL_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC15_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') & (BAM095_adj['CONTRACT_AMT1']<=500000)]['TOTAL_AMT'].agg('count')]
            BAM095_VAR['CASHCARD_JCC18_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') ]['TOTAL_AMT'].agg('sum')]
            
            BAM095_VAR['CASHCARD_JCC44_1']=np.select([BAM095_VAR['CASHCARD_JCC9_1']>0 ,BAM095_VAR['CASHCARD_JCC18_1']==0],[BAM095_VAR['CASHCARD_JCC18_1']/BAM095_VAR['CASHCARD_JCC9_1'],0],default=9999)
            BAM095_adj['tmp']=np.select([(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['CONTRACT_AMT']==0) & (BAM095_adj['TOTAL_AMT']>0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['TOTAL_AMT']==0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & BAM095_adj['TOTAL_AMT']/BAM095_adj['CONTRACT_AMT']>=0.3],[1,0,1],default=0)
            BAM095_VAR['CASHCARD_JCC51_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') ]['tmp'].agg('sum')]
            BAM095_adj['tmp']=np.select([(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['CONTRACT_AMT']==0) & (BAM095_adj['TOTAL_AMT']>0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['TOTAL_AMT']==0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & BAM095_adj['TOTAL_AMT']/BAM095_adj['CONTRACT_AMT']>=0.5],[1,0,1],default=0)
            BAM095_VAR['CASHCARD_JCC52_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') ]['tmp'].agg('sum')]
            BAM095_adj['tmp']=np.select([(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['CONTRACT_AMT']==0) & (BAM095_adj['TOTAL_AMT']>0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['TOTAL_AMT']==0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & BAM095_adj['TOTAL_AMT']/BAM095_adj['CONTRACT_AMT']>=0.7],[1,0,1],default=0)
            BAM095_VAR['CASHCARD_JCC53_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') ]['tmp'].agg('sum')]
            BAM095_adj['tmp']=np.select([(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['CONTRACT_AMT']==0) & (BAM095_adj['TOTAL_AMT']>0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & (BAM095_adj['TOTAL_AMT']==0),(BAM095_adj['CASHCARD_FLAG']=='Y')  & BAM095_adj['TOTAL_AMT']/BAM095_adj['CONTRACT_AMT']>=0.8],[1,0,1],default=0)
            BAM095_VAR['CASHCARD_JCC54_1']=[BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') ]['tmp'].agg('sum')]
            
            ##銀行家數
            BAM095_VAR['JLN26_1']=len(BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_1']=len(BAM095_adj.loc[(BAM095_adj['LOAN_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_2']=len(BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_2']=len(BAM095_adj.loc[(BAM095_adj['LN_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_3']=len(BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_3']=len(BAM095_adj.loc[(BAM095_adj['AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_4']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_4']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_5']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_5']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_6']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_6']=len(BAM095_adj.loc[(BAM095_adj['UNSEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_7']=len(BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_7']=len(BAM095_adj.loc[(BAM095_adj['PL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_8']=len(BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_8']=len(BAM095_adj.loc[(BAM095_adj['SL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_9']=len(BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_9']=len(BAM095_adj.loc[(BAM095_adj['SEC_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_10']=len(BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_10']=len(BAM095_adj.loc[(BAM095_adj['SEC_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_11']=len(BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_11']=len(BAM095_adj.loc[(BAM095_adj['SEC_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_12']=len(BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_12']=len(BAM095_adj.loc[(BAM095_adj['SECURITY_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_13']=len(BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_13']=len(BAM095_adj.loc[(BAM095_adj['MP_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_14']=len(BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_14']=len(BAM095_adj.loc[(BAM095_adj['AL_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_15']=len(BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_15']=len(BAM095_adj.loc[(BAM095_adj['RE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_16']=len(BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_16']=len(BAM095_adj.loc[(BAM095_adj['MTG_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_17']=len(BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_17']=len(BAM095_adj.loc[(BAM095_adj['MTG_CYCLE_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_18']=len(BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_18']=len(BAM095_adj.loc[(BAM095_adj['MTG_AMORT_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN26_21']=len(BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y')].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            BAM095_VAR['JLN27_21']=len(BAM095_adj.loc[(BAM095_adj['CASHCARD_FLAG']=='Y') & (BAM095_adj['TOTAL_AMT']>0)].groupby(['CUSTOMER_ID','BANK_NO'], as_index=False).agg('count'))
            
            
            # ##### KRM040
            ##欄位名稱調整 新增底層欄位
            KRM040=KRM040.rename(
                columns={
                        'SYSDATE':'QUERY_DATE',
                        'IDN_BAN':'CUSTOMER_ID'
                    }
                )
            KRM040['CARD_TYPE_ADJ']=KRM040['CARD_TYPE'].str.strip()
            
            KRM040['PERM_LIMIT']=KRM040['PERM_LIMIT']*1000
            KRM040['TEMP_LIMIT']=KRM040['TEMP_LIMIT']*1000
            KRM040['CASH_LIMIT']=KRM040['CASH_LIMIT']*1000
            KRM040['PAY_CODE_ADJ']=np.select([KRM040['PAY_CODE'].isin(['','N','X']),(KRM040['PAY_CODE']=='1') & KRM040['PAY_STAT'].isin(['3','4']),(KRM040['PAY_CODE']=='1')],[0,1,0],default=KRM040['PAY_CODE'])
            
            
            KRM040['QUERY_DATE']=pd.to_datetime(KRM040['QUERY_DATE'])
            KRM040['BILL_DATE']=pd.to_datetime(KRM040['BILL_DATE'])
            KRM040['rnk']=KRM040.sort_values(['BILL_DATE'], ascending=[False]).groupby(['ISSUE','BILL_MARK']).cumcount() + 1
            KRM040['max_BILL_DATE']=KRM040.groupby(['ISSUE','BILL_MARK'])['BILL_DATE'].transform('max')
            KRM040['period_adj']=np.where((KRM040['QUERY_DATE']-KRM040['max_BILL_DATE']).dt.days<=45,KRM040['rnk'],np.floor((KRM040['QUERY_DATE']-KRM040['max_BILL_DATE']).dt.days/30)+KRM040['rnk'])
            KRM040['PERM_LIMIT_UNDER_5']=np.where(KRM040['PERM_LIMIT']<=50,1,0)
            KRM040['PERM_LIMIT_OVER_30']=np.where(KRM040['PERM_LIMIT']>=300,1,0)
            KRM040['PERM_LIMIT_OVER_50']=np.where(KRM040['PERM_LIMIT']>=500,1,0)
            KRM040['PAYABLE_M1P']=np.where(KRM040['PAY_CODE_ADJ'].astype(int)>=1,KRM040['PAYABLE'],0)
            KRM040['USAGE_PAYABLE_PRE_OWED']=np.select([(KRM040['PERM_LIMIT']==0) & (KRM040['PAYABLE']+KRM040['PRE_OWED']>0),(KRM040['PERM_LIMIT']==0) & (KRM040['PAYABLE']+KRM040['PRE_OWED']<=0)],[9999,0],default=(KRM040['PAYABLE']+KRM040['PRE_OWED'])/KRM040['PERM_LIMIT'])
            KRM040['USAGE_REVOL_BAL_PRE_OWED']=np.select([(KRM040['PERM_LIMIT']==0) & (KRM040['REVOL_BAL']+KRM040['PRE_OWED']>0),(KRM040['PERM_LIMIT']==0) & (KRM040['REVOL_BAL']+KRM040['PRE_OWED']<=0)],[9999,0],default=(KRM040['REVOL_BAL']+KRM040['PRE_OWED'])/KRM040['PERM_LIMIT'])
            KRM040['PRE_OWED_CNT']=np.where(KRM040['PRE_OWED']>=0,1,0)
            
            KRM040['CARD_CNT']=KRM040['CARD_TYPE_ADJ'].str.len()
            KRM040['SUM_PERM_LIMIT']=KRM040['PERM_LIMIT']
            KRM040['MAX_PERM_LIMIT']=KRM040['PERM_LIMIT']
            KRM040['MIN_PERM_LIMIT']=KRM040['PERM_LIMIT']
            KRM040['SUM_TEMP_LIMIT']=KRM040['TEMP_LIMIT']
            KRM040['MAX_TEMP_LIMIT']=KRM040['TEMP_LIMIT']
            KRM040['MAX_ADJ_LIMIT']=KRM040['TEMP_LIMIT']-KRM040['PERM_LIMIT']
            
            
            KRM040['SUM_CASH_LIMIT']=KRM040['CASH_LIMIT']
            KRM040['SUM_PAYABLE']=KRM040['PAYABLE']
            KRM040['MAX_PAYABLE']=KRM040['PAYABLE']
            KRM040['SUM_CASH_LENT']=KRM040['CASH_LENT']
            KRM040['SUM_LAST_PAYA']=KRM040['LAST_PAYA']
            KRM040['SUM_REVOL_BAL']=KRM040['REVOL_BAL']
            KRM040['MAX_REVOL_BAL']=KRM040['REVOL_BAL']
            KRM040['SUM_PRE_OWED']=KRM040['PRE_OWED']
            KRM040['SUM_PAYABLE_M1P']=KRM040['PAYABLE_M1P']
            KRM040['USAGE_PAYABLE_PRE_OWED_30']=np.where(KRM040['USAGE_PAYABLE_PRE_OWED']>=0.3,1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_50']=np.where(KRM040['USAGE_PAYABLE_PRE_OWED']>=0.5,1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_60']=np.where(KRM040['USAGE_PAYABLE_PRE_OWED']>=0.6,1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_70']=np.where(KRM040['USAGE_PAYABLE_PRE_OWED']>=0.7,1,0)
            KRM040['USAGE_REVOL_PRE_OWED_30']=np.where(KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.3,1,0)
            KRM040['USAGE_REVOL_PRE_OWED_50']=np.where(KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.5,1,0)
            KRM040['USAGE_REVOL_PRE_OWED_70']=np.where(KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.7,1,0)
            KRM040['USAGE_REVOL_PRE_OWED_80']=np.where(KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.8,1,0)
            KRM040['SUM_PERM_LIMIT_WO_A82']=np.where(KRM040['ISSUE']!='A82',KRM040['PERM_LIMIT'],0)
            KRM040['SUM_PAYABLE_WO_A82']=np.where(KRM040['ISSUE']!='A82',KRM040['PAYABLE'],0)
            KRM040['SUM_REVOL_BAL_WO_A82']=np.where(KRM040['ISSUE']!='A82',KRM040['REVOL_BAL'],0)
            KRM040['SUM_PRE_OWED_WO_A82']=np.where(KRM040['ISSUE']!='A82',KRM040['PRE_OWED'],0)
            KRM040['BILL_CNT_WO_A82']=np.where(KRM040['ISSUE']!='A82',1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_30_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_PAYABLE_PRE_OWED']>=0.3),1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_50_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_PAYABLE_PRE_OWED']>=0.5),1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_60_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_PAYABLE_PRE_OWED']>=0.6),1,0)
            KRM040['USAGE_PAYABLE_PRE_OWED_70_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_PAYABLE_PRE_OWED']>=0.7),1,0)
            KRM040['USAGE_REVOL_PRE_OWED_30_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.3),1,0)
            KRM040['USAGE_REVOL_PRE_OWED_50_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.5),1,0)
            KRM040['USAGE_REVOL_PRE_OWED_70_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.7),1,0)
            KRM040['USAGE_REVOL_PRE_OWED_80_WO_A82']=np.where((KRM040['ISSUE']!='A82') & (KRM040['USAGE_REVOL_BAL_PRE_OWED']>=0.8),1,0)
            
            ##GROUBY 到ID 銀行 帳單期數
            KRM040_adj=KRM040.groupby(['CUSTOMER_ID','period_adj','ISSUE'], as_index=False).agg({'rnk':'count'
                                                                                                ,'CARD_CNT':'sum'
                                                                                                ,'SUM_PERM_LIMIT':'sum'
                                                                                                ,'MAX_PERM_LIMIT':'max'
                                                                                                ,'MIN_PERM_LIMIT':'min'
                                                                                                ,'SUM_TEMP_LIMIT':'sum'
                                                                                                ,'MAX_TEMP_LIMIT':'max'
                                                                                                ,'MAX_ADJ_LIMIT':'max'
                                                                                                ,'SUM_CASH_LIMIT':'sum'
                                                                                                ,'SUM_PAYABLE':'sum'
                                                                                                ,'MAX_PAYABLE':'max'
                                                                                                ,'SUM_CASH_LENT':'sum'
                                                                                                ,'SUM_LAST_PAYA':'sum'
                                                                                                ,'SUM_REVOL_BAL':'sum'
                                                                                                ,'MAX_REVOL_BAL':'max'
                                                                                                ,'PAY_CODE_ADJ':'max'
                                                                                                ,'SUM_PRE_OWED':'sum'
                                                                                                ,'PERM_LIMIT_UNDER_5':'sum'
                                                                                                ,'PERM_LIMIT_OVER_30':'sum'
                                                                                                ,'PERM_LIMIT_OVER_50':'sum'
                                                                                                ,'SUM_PAYABLE_M1P':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_30':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_50':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_60':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_70':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_30':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_50':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_70':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_80':'sum'
                                                                                                 ,'PRE_OWED_CNT':'sum'
                                                                                                 ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                                 ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                                 ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                                 ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                                 ,'BILL_CNT_WO_A82':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_30_WO_A82':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_50_WO_A82':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_60_WO_A82':'sum'
                                                                                                 ,'USAGE_PAYABLE_PRE_OWED_70_WO_A82':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_30_WO_A82':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_50_WO_A82':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_70_WO_A82':'sum'
                                                                                                 ,'USAGE_REVOL_PRE_OWED_80_WO_A82':'sum'
                                                                                                })
            KRM040_adj=KRM040_adj.rename(columns={'rnk':'BILL_CNT'})
            
            KRM040_adj['PURCHASE_FLAG_BY_BANK']=np.where(KRM040_adj['SUM_PAYABLE']-KRM040_adj['SUM_REVOL_BAL']>0,1,0)
            KRM040_adj['NET_PURCHASE_FLAG_BY_BANK']=np.where(KRM040_adj['SUM_PAYABLE']-KRM040_adj['SUM_REVOL_BAL']-KRM040_adj['SUM_CASH_LENT']>0,1,0)
            KRM040_adj['CASH_LENT_FLAG_BY_BANK']=np.where(KRM040_adj['SUM_CASH_LENT']>0,1,0)
            
            
            ##GROUBY 到ID 帳單期數
            KRM040_byPERIOD=KRM040_adj.groupby(['CUSTOMER_ID','period_adj'], as_index=False).agg({'ISSUE':'count'
                                                                                             ,'BILL_CNT':'sum'
                                                                                             ,'CARD_CNT':'sum'
                                                                                             ,'SUM_PERM_LIMIT':'sum'
                                                                                             ,'MAX_PERM_LIMIT':'max'
                                                                                             ,'MIN_PERM_LIMIT':'min'
                                                                                             ,'SUM_TEMP_LIMIT':'sum'
                                                                                             ,'MAX_TEMP_LIMIT':'max'
                                                                                             ,'MAX_ADJ_LIMIT':'max'
                                                                                             ,'SUM_CASH_LIMIT':'sum'
                                                                                             ,'SUM_PAYABLE':'sum'
                                                                                             ,'MAX_PAYABLE':'max'
                                                                                             ,'SUM_CASH_LENT':'sum'
                                                                                             ,'SUM_LAST_PAYA':'sum'
                                                                                             ,'SUM_REVOL_BAL':'sum'
                                                                                             ,'MAX_REVOL_BAL':'max'
                                                                                             ,'PAY_CODE_ADJ':'max'
                                                                                             ,'SUM_PRE_OWED':'sum'
                                                                                             ,'PERM_LIMIT_UNDER_5':'sum'
                                                                                             ,'PERM_LIMIT_OVER_30':'sum'
                                                                                             ,'PERM_LIMIT_OVER_50':'sum'
                                                                                             ,'SUM_PAYABLE_M1P':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_30':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_50':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_60':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_70':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_30':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_50':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_70':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_80':'sum'
                                                                                             ,'PURCHASE_FLAG_BY_BANK':'sum'
                                                                                             ,'PRE_OWED_CNT':'sum'
                                                                                             ,'CASH_LENT_FLAG_BY_BANK':'sum'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                             ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                             ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                             ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                             ,'BILL_CNT_WO_A82':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_30_WO_A82':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_50_WO_A82':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_60_WO_A82':'sum'
                                                                                             ,'USAGE_PAYABLE_PRE_OWED_70_WO_A82':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_30_WO_A82':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_50_WO_A82':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_70_WO_A82':'sum'
                                                                                             ,'USAGE_REVOL_PRE_OWED_80_WO_A82':'sum'
                                                                                            })
            KRM040_byPERIOD=KRM040_byPERIOD.rename(columns={'ISSUE':'BANK_CNT'
                                                    ,'PURCHASE_FLAG_BY_BANK':'HAS_PURCHASE_BANK_CNT'
                                                    ,'CASH_LENT_FLAG_BY_BANK':'HAS_CASH_LENTE_BANK_CNT'
                                                   })
            KRM040_byPERIOD['AVG_PERM_LIMIT']=np.where(KRM040_byPERIOD['BILL_CNT']==0,0,KRM040_byPERIOD['SUM_PERM_LIMIT']/KRM040_byPERIOD['BILL_CNT'])
            KRM040_byPERIOD['PAYABLE_FLAG']=np.where(KRM040_byPERIOD['SUM_PAYABLE']>0,1,0)
            KRM040_byPERIOD['PAYABLE_PREOWED_FLAG']=np.where(KRM040_byPERIOD['SUM_PAYABLE']+KRM040_byPERIOD['SUM_PRE_OWED']>0,1,0)
            KRM040_byPERIOD['REVOL_FLAG']=np.where(KRM040_byPERIOD['SUM_REVOL_BAL']>0,1,0)
            KRM040_byPERIOD['REVOL_PREOWED_FLAG']=np.where(KRM040_byPERIOD['SUM_REVOL_BAL']+KRM040_byPERIOD['SUM_PRE_OWED']>0,1,0)
            KRM040_byPERIOD['PURCHASE_FLAG']=np.where(KRM040_byPERIOD['SUM_PAYABLE']-KRM040_byPERIOD['SUM_REVOL_BAL']>0,1,0)
            KRM040_byPERIOD['NET_PURCHASE_FLAG']=np.where(KRM040_byPERIOD['SUM_PAYABLE']-KRM040_byPERIOD['SUM_REVOL_BAL']-KRM040_byPERIOD['SUM_CASH_LENT']>0,1,0)
            KRM040_byPERIOD['PRE_OWED_FLAG']=np.where(KRM040_byPERIOD['SUM_PRE_OWED']>0,1,0)
            KRM040_byPERIOD['CASH_LENT_FLAG']=np.where(KRM040_byPERIOD['SUM_CASH_LENT']>0,1,0)
            
            
            ##GROUBY 到ID
            KRM040_byPERIOD_1_1=KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==1]
            KRM040_byPERIOD_2_2=KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==2]
            KRM040_byPERIOD_1_3=KRM040_byPERIOD.loc[(KRM040_byPERIOD['period_adj']>=1) & (KRM040_byPERIOD['period_adj']<=3)]
            KRM040_byPERIOD_4_6=KRM040_byPERIOD.loc[(KRM040_byPERIOD['period_adj']>=4) & (KRM040_byPERIOD['period_adj']<=6)]
            KRM040_byPERIOD_1_6=KRM040_byPERIOD.loc[(KRM040_byPERIOD['period_adj']>=1) & (KRM040_byPERIOD['period_adj']<=6)]
            KRM040_byPERIOD_7_12=KRM040_byPERIOD.loc[(KRM040_byPERIOD['period_adj']>=7) & (KRM040_byPERIOD['period_adj']<=12)]
            KRM040_byPERIOD_1_12=KRM040_byPERIOD.loc[(KRM040_byPERIOD['period_adj']>=1) & (KRM040_byPERIOD['period_adj']<=12)]
            
            #永久額度
            KRM040_VAR_1_1=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                           ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                           ,'SUM_PAYABLE_M1P':'sum'
                                                                                           ,'SUM_PAYABLE':'sum'
                                                                                           ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                           ,'SUM_PRE_OWED':'sum'
                                                                                           ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                           ,'SUM_REVOL_BAL':'sum'
                                                                                           ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                           ,'SUM_CASH_LENT':'sum'
                                                                                           ,'PAYABLE_PREOWED_FLAG':'sum'
                                                                                           ,'REVOL_PREOWED_FLAG':'sum'
                                                                                           ,'PURCHASE_FLAG':'sum'
                                                                                           ,'NET_PURCHASE_FLAG':'sum'
                                                                                           ,'PRE_OWED_FLAG':'sum'
                                                                                           ,'period_adj':'count'
                                                                                           ,'PAY_CODE_ADJ':'max'
                                                                                          })
            KRM040_VAR=KRM040_VAR_1_1.rename(columns={'SUM_PERM_LIMIT':'SUM_PERM_LIMIT_1'
                                                      ,'SUM_PERM_LIMIT_WO_A82':'SUM_PERM_LIMIT_WO_A82_1'
                                                      ,'SUM_PAYABLE_M1P':'SUM_PAYABLE_M1P_1'
                                                      ,'SUM_PAYABLE':'SUM_PAYABLE_1'
                                                      ,'SUM_PAYABLE_WO_A82':'SUM_PAYABLE_WO_A82_1'
                                                      ,'SUM_PRE_OWED':'SUM_PRE_OWED_1'
                                                      ,'SUM_PRE_OWED_WO_A82':'SUM_PRE_OWED_WO_A82_1'
                                                      ,'SUM_REVOL_BAL':'SUM_REVOL_BAL_1'
                                                      ,'SUM_REVOL_BAL_WO_A82':'SUM_REVOL_BAL_WO_A82_1'
                                                      ,'SUM_CASH_LENT':'SUM_CASH_LENT_1'
                                                      ,'PAYABLE_PREOWED_FLAG':'PAYABLE_PREOWED_CNT_1'
                                                      ,'REVOL_PREOWED_FLAG':'REVOL_PREOWED_CNT_1'
                                                      ,'PURCHASE_FLAG':'PURCHASE_CNT_1'
                                                      ,'NET_PURCHASE_FLAG':'NET_PURCHASE_CNT_1'
                                                      ,'PRE_OWED_FLAG':'PREOWED_CNT_1'
                                                      ,'period_adj':'PERIOD_CNT_1'
                                                      ,'PAY_CODE_ADJ':'PAY_CODE_ADJ_1'
                                                 })
            
            
            KRM040_VAR[['SUM_PERM_LIMIT_2'
                        ,'SUM_PERM_LIMIT_WO_A82_2'
                        ,'SUM_PAYABLE_2'
                        ,'SUM_PAYABLE_WO_A82_2'
                        ,'SUM_PRE_OWED_2'
                        ,'SUM_PRE_OWED_WO_A82_2'
                        ,'SUM_REVOL_BAL_2'
                        ,'SUM_REVOL_BAL_WO_A82_2'
                        ,'PERIOD_CNT_2'
                       ]]=KRM040_byPERIOD_2_2.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                          ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                          ,'SUM_PAYABLE':'sum'
                                                                                          ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                          ,'SUM_PRE_OWED':'sum'
                                                                                          ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                          ,'SUM_REVOL_BAL':'sum'
                                                                                          ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                          ,'period_adj':'count'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'period_adj'
                                                                                            ]]
            KRM040_VAR[['SUM_PERM_LIMIT_1_3'
                        ,'SUM_PERM_LIMIT_WO_A82_1_3'
                        ,'SUM_PAYABLE_M1P_1_3'
                        ,'SUM_PAYABLE_1_3'
                        ,'SUM_PAYABLE_WO_A82_1_3'
                        ,'SUM_PRE_OWED_1_3'
                        ,'SUM_PRE_OWED_WO_A82_1_3'
                        ,'SUM_REVOL_BAL_1_3'
                        ,'SUM_REVOL_BAL_WO_A82_1_3'
                        ,'SUM_CASH_LENT_1_3'
                        ,'PAYABLE_CNT_1_3'
                        ,'REVOL_BAL_CNT_1_3'
                        ,'PAYABLE_PREOWED_CNT_1_3'
                        ,'REVOL_PREOWED_CNT_1_3'
                        ,'PURCHASE_CNT_1_3'
                        ,'NET_PURCHASE_CNT_1_3'
                        ,'PREOWED_CNT_1_3'
                        ,'PERIOD_CNT_1_3'
                        ,'PAY_CODE_ADJ_1_3'
                      ]]=KRM040_byPERIOD_1_3.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                          ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                          ,'SUM_PAYABLE_M1P':'sum'
                                                                                          ,'SUM_PAYABLE':'sum'
                                                                                          ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                          ,'SUM_PRE_OWED':'sum'
                                                                                          ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                          ,'SUM_REVOL_BAL':'sum'
                                                                                          ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                          ,'SUM_CASH_LENT':'sum'
                                                                                          ,'PAYABLE_FLAG':'sum'
                                                                                          ,'REVOL_FLAG':'sum'
                                                                                          ,'PAYABLE_PREOWED_FLAG':'sum'
                                                                                          ,'REVOL_PREOWED_FLAG':'sum'
                                                                                          ,'PURCHASE_FLAG':'sum'
                                                                                          ,'NET_PURCHASE_FLAG':'sum'
                                                                                          ,'PRE_OWED_FLAG':'sum'
                                                                                          ,'period_adj':'count'
                                                                                          ,'PAY_CODE_ADJ':'max'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE_M1P'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'SUM_CASH_LENT'
                                                                                             ,'PAYABLE_FLAG'
                                                                                             ,'REVOL_FLAG'
                                                                                             ,'PAYABLE_PREOWED_FLAG'
                                                                                             ,'REVOL_PREOWED_FLAG'
                                                                                             ,'PURCHASE_FLAG'
                                                                                             ,'NET_PURCHASE_FLAG'
                                                                                             ,'PRE_OWED_FLAG'
                                                                                             ,'period_adj'
                                                                                             ,'PAY_CODE_ADJ'
                                                                                            ]]
            KRM040_VAR[['SUM_PERM_LIMIT_4_6'
                        ,'SUM_PERM_LIMIT_WO_A82_4_6'
                        ,'SUM_PAYABLE_4_6'
                        ,'SUM_PAYABLE_WO_A82_4_6'
                        ,'SUM_PRE_OWED_4_6'
                        ,'SUM_PRE_OWED_WO_A82_4_6'
                        ,'SUM_REVOL_BAL_4_6'
                        ,'SUM_REVOL_BAL_WO_A82_4_6'
                        ,'PERIOD_CNT_4_6'
                       ]]=KRM040_byPERIOD_4_6.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                          ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                          ,'SUM_PAYABLE':'sum'
                                                                                          ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                          ,'SUM_PRE_OWED':'sum'
                                                                                          ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                          ,'SUM_REVOL_BAL':'sum'
                                                                                          ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                          ,'period_adj':'count'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'period_adj'
                                                                                            ]]
            KRM040_VAR[['SUM_PERM_LIMIT_1_6'
                        ,'SUM_PERM_LIMIT_WO_A82_1_6'
                        ,'SUM_PAYABLE_M1P_1_6'
                        ,'SUM_PAYABLE_1_6'
                        ,'SUM_PAYABLE_WO_A82_1_6'
                        ,'SUM_PRE_OWED_1_6'
                        ,'SUM_PRE_OWED_WO_A82_1_6'
                        ,'SUM_REVOL_BAL_1_6'
                        ,'SUM_REVOL_BAL_WO_A82_1_6'
                        ,'SUM_CASH_LENT_1_6'
                        ,'PAYABLE_CNT_1_6'
                        ,'REVOL_BAL_CNT_1_6'
                        ,'PAYABLE_PREOWED_CNT_1_6'
                        ,'REVOL_PREOWED_CNT_1_6'
                        ,'PURCHASE_CNT_1_6'
                        ,'NET_PURCHASE_CNT_1_6'
                        ,'PREOWED_CNT_1_6'
                        ,'PERIOD_CNT_1_6'
                        ,'PAY_CODE_ADJ_1_6'
                       ]]=KRM040_byPERIOD_1_6.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                          ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                          ,'SUM_PAYABLE_M1P':'sum'
                                                                                          ,'SUM_PAYABLE':'sum'
                                                                                          ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                          ,'SUM_PRE_OWED':'sum'
                                                                                          ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                          ,'SUM_REVOL_BAL':'sum'
                                                                                          ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                          ,'SUM_CASH_LENT':'sum'
                                                                                          ,'PAYABLE_FLAG':'sum'
                                                                                          ,'REVOL_FLAG':'sum'
                                                                                          ,'PAYABLE_PREOWED_FLAG':'sum'
                                                                                          ,'REVOL_PREOWED_FLAG':'sum'
                                                                                          ,'PURCHASE_FLAG':'sum'
                                                                                          ,'NET_PURCHASE_FLAG':'sum'
                                                                                          ,'PRE_OWED_FLAG':'sum'
                                                                                          ,'period_adj':'count'
                                                                                          ,'PAY_CODE_ADJ':'max'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE_M1P'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'SUM_CASH_LENT'
                                                                                             ,'PAYABLE_FLAG'
                                                                                             ,'REVOL_FLAG'
                                                                                             ,'PAYABLE_PREOWED_FLAG'
                                                                                             ,'REVOL_PREOWED_FLAG'
                                                                                             ,'PURCHASE_FLAG'
                                                                                             ,'NET_PURCHASE_FLAG'
                                                                                             ,'PRE_OWED_FLAG'
                                                                                             ,'period_adj'
                                                                                             ,'PAY_CODE_ADJ'
                                                                                            ]]
            KRM040_VAR[['SUM_PERM_LIMIT_7_12'
                        ,'SUM_PERM_LIMIT_WO_A82_7_12'
                        ,'SUM_PAYABLE_7_12'
                        ,'SUM_PAYABLE_WO_A82_7_12'
                        ,'SUM_PRE_OWED_7_12'
                        ,'SUM_PRE_OWED_WO_A82_7_12'
                        ,'SUM_REVOL_BAL_7_12'
                        ,'SUM_REVOL_BAL_WO_A82_7_12'
                        ,'PERIOD_CNT_7_12'
                       ]]=KRM040_byPERIOD_7_12.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                           ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                           ,'SUM_PAYABLE':'sum'
                                                                                           ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                           ,'SUM_PRE_OWED':'sum'
                                                                                           ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                           ,'SUM_REVOL_BAL':'sum'
                                                                                           ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                           ,'period_adj':'count'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'period_adj'
                                                                                            ]]
            KRM040_VAR[['SUM_PERM_LIMIT_1_12'
                        ,'SUM_PERM_LIMIT_WO_A82_1_12'
                        ,'SUM_PAYABLE_M1P_1_12'
                        ,'SUM_PAYABLE_1_12'
                        ,'SUM_PAYABLE_WO_A82_1_12'
                        ,'SUM_PRE_OWED_1_12'
                        ,'SUM_PRE_OWED_WO_A82_1_12'
                        ,'SUM_REVOL_BAL_1_12'
                        ,'SUM_REVOL_BAL_WO_A82_1_12'
                        ,'SUM_CASH_LENT_1_12'
                        ,'PAYABLE_CNT_1_12'
                        ,'REVOL_BAL_CNT_1_12'
                        ,'PAYABLE_PREOWED_CNT_1_12'
                        ,'REVOL_PREOWED_CNT_1_12'
                        ,'PURCHASE_CNT_1_12'
                        ,'NET_PURCHASE_CNT_1_12'
                        ,'PREOWED_CNT_1_12'
                        ,'PERIOD_CNT_1_12'
                        ,'PAY_CODE_ADJ_1_12'
                       ]]=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'SUM_PERM_LIMIT':'sum'
                                                                                          ,'SUM_PERM_LIMIT_WO_A82':'sum'
                                                                                          ,'SUM_PAYABLE_M1P':'sum'
                                                                                          ,'SUM_PAYABLE':'sum'
                                                                                          ,'SUM_PAYABLE_WO_A82':'sum'
                                                                                          ,'SUM_PRE_OWED':'sum'
                                                                                          ,'SUM_PRE_OWED_WO_A82':'sum'
                                                                                          ,'SUM_REVOL_BAL':'sum'
                                                                                          ,'SUM_REVOL_BAL_WO_A82':'sum'
                                                                                          ,'SUM_CASH_LENT':'sum'
                                                                                          ,'PAYABLE_FLAG':'sum'
                                                                                          ,'REVOL_FLAG':'sum'
                                                                                          ,'PAYABLE_PREOWED_FLAG':'sum'
                                                                                          ,'REVOL_PREOWED_FLAG':'sum'
                                                                                          ,'PURCHASE_FLAG':'sum'
                                                                                          ,'NET_PURCHASE_FLAG':'sum'
                                                                                          ,'PRE_OWED_FLAG':'sum'
                                                                                          ,'period_adj':'count'
                                                                                          ,'PAY_CODE_ADJ':'max'
                                                                                         })[['SUM_PERM_LIMIT'
                                                                                             ,'SUM_PERM_LIMIT_WO_A82'
                                                                                             ,'SUM_PAYABLE_M1P'
                                                                                             ,'SUM_PAYABLE'
                                                                                             ,'SUM_PAYABLE_WO_A82'
                                                                                             ,'SUM_PRE_OWED'
                                                                                             ,'SUM_PRE_OWED_WO_A82'
                                                                                             ,'SUM_REVOL_BAL'
                                                                                             ,'SUM_REVOL_BAL_WO_A82'
                                                                                             ,'SUM_CASH_LENT'
                                                                                             ,'PAYABLE_FLAG'
                                                                                             ,'REVOL_FLAG'
                                                                                             ,'PAYABLE_PREOWED_FLAG'
                                                                                             ,'REVOL_PREOWED_FLAG'
                                                                                             ,'PURCHASE_FLAG'
                                                                                             ,'NET_PURCHASE_FLAG'
                                                                                             ,'PRE_OWED_FLAG'
                                                                                             ,'period_adj'
                                                                                             ,'PAY_CODE_ADJ'
                                                                                            ]]
            #帳單+分期餘額
            KRM040_VAR['SUM_PAYABLE_PREOWED_1']=KRM040_VAR['SUM_PAYABLE_1']+KRM040_VAR['SUM_PRE_OWED_1']
            KRM040_VAR['SUM_PAYABLE_PREOWED_2']=KRM040_VAR['SUM_PAYABLE_2']+KRM040_VAR['SUM_PRE_OWED_2']
            KRM040_VAR['SUM_PAYABLE_PREOWED_1_3']=KRM040_VAR['SUM_PAYABLE_1_3']+KRM040_VAR['SUM_PRE_OWED_1_3']
            KRM040_VAR['SUM_PAYABLE_PREOWED_1_6']=KRM040_VAR['SUM_PAYABLE_1_6']+KRM040_VAR['SUM_PRE_OWED_1_6']
            KRM040_VAR['SUM_PAYABLE_PREOWED_4_6']=KRM040_VAR['SUM_PAYABLE_4_6']+KRM040_VAR['SUM_PRE_OWED_4_6']
            KRM040_VAR['SUM_PAYABLE_PREOWED_1_12']=KRM040_VAR['SUM_PAYABLE_1_12']+KRM040_VAR['SUM_PRE_OWED_1_12']
            KRM040_VAR['SUM_PAYABLE_PREOWED_7_12']=KRM040_VAR['SUM_PAYABLE_7_12']+KRM040_VAR['SUM_PRE_OWED_7_12']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1']=KRM040_VAR['SUM_PAYABLE_WO_A82_1']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_2']=KRM040_VAR['SUM_PAYABLE_WO_A82_2']+KRM040_VAR['SUM_PRE_OWED_WO_A82_2']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_3']=KRM040_VAR['SUM_PAYABLE_WO_A82_1_3']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_3']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_6']=KRM040_VAR['SUM_PAYABLE_WO_A82_1_6']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_6']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_4_6']=KRM040_VAR['SUM_PAYABLE_WO_A82_4_6']+KRM040_VAR['SUM_PRE_OWED_WO_A82_4_6']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_12']=KRM040_VAR['SUM_PAYABLE_WO_A82_1_12']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_12']
            KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_7_12']=KRM040_VAR['SUM_PAYABLE_WO_A82_7_12']+KRM040_VAR['SUM_PRE_OWED_WO_A82_7_12']
            #循環+分期餘額
            KRM040_VAR['SUM_REVOL_PREOWED_1']=KRM040_VAR['SUM_REVOL_BAL_1']+KRM040_VAR['SUM_PRE_OWED_1']
            KRM040_VAR['SUM_REVOL_PREOWED_2']=KRM040_VAR['SUM_REVOL_BAL_2']+KRM040_VAR['SUM_PRE_OWED_2']
            KRM040_VAR['SUM_REVOL_PREOWED_1_3']=KRM040_VAR['SUM_REVOL_BAL_1_3']+KRM040_VAR['SUM_PRE_OWED_1_3']
            KRM040_VAR['SUM_REVOL_PREOWED_1_6']=KRM040_VAR['SUM_REVOL_BAL_1_6']+KRM040_VAR['SUM_PRE_OWED_1_6']
            KRM040_VAR['SUM_REVOL_PREOWED_4_6']=KRM040_VAR['SUM_REVOL_BAL_4_6']+KRM040_VAR['SUM_PRE_OWED_4_6']
            KRM040_VAR['SUM_REVOL_PREOWED_1_12']=KRM040_VAR['SUM_REVOL_BAL_1_12']+KRM040_VAR['SUM_PRE_OWED_1_12']
            KRM040_VAR['SUM_REVOL_PREOWED_7_12']=KRM040_VAR['SUM_REVOL_BAL_7_12']+KRM040_VAR['SUM_PRE_OWED_7_12']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_1']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_2']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_2']+KRM040_VAR['SUM_PRE_OWED_2']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_3']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_1_3']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_3']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_6']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_1_6']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_6']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_4_6']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_4_6']+KRM040_VAR['SUM_PRE_OWED_WO_A82_4_6']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_12']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_1_12']+KRM040_VAR['SUM_PRE_OWED_WO_A82_1_12']
            KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_7_12']=KRM040_VAR['SUM_REVOL_BAL_WO_A82_7_12']+KRM040_VAR['SUM_PRE_OWED_WO_A82_7_12']
            #消費(帳單-循環)
            KRM040_VAR['SUM_PURCHASE_1']=KRM040_VAR['SUM_PAYABLE_1']-KRM040_VAR['SUM_REVOL_BAL_1']
            KRM040_VAR['SUM_PURCHASE_1_3']=KRM040_VAR['SUM_PAYABLE_1_3']-KRM040_VAR['SUM_REVOL_BAL_1_3']
            KRM040_VAR['SUM_PURCHASE_1_6']=KRM040_VAR['SUM_PAYABLE_1_6']-KRM040_VAR['SUM_REVOL_BAL_1_6']
            KRM040_VAR['SUM_PURCHASE_1_12']=KRM040_VAR['SUM_PAYABLE_1_12']-KRM040_VAR['SUM_REVOL_BAL_1_12']
            #淨消費(帳單-循環-預現)
            KRM040_VAR['SUM_NET_PURCHASE_1']=KRM040_VAR['SUM_PAYABLE_1']-KRM040_VAR['SUM_REVOL_BAL_1']-KRM040_VAR['SUM_CASH_LENT_1']
            KRM040_VAR['SUM_NET_PURCHASE_1_3']=KRM040_VAR['SUM_PAYABLE_1_3']-KRM040_VAR['SUM_REVOL_BAL_1_3']-KRM040_VAR['SUM_CASH_LENT_1_3']
            KRM040_VAR['SUM_NET_PURCHASE_1_6']=KRM040_VAR['SUM_PAYABLE_1_6']-KRM040_VAR['SUM_REVOL_BAL_1_6']-KRM040_VAR['SUM_CASH_LENT_1_6']
            KRM040_VAR['SUM_NET_PURCHASE_1_12']=KRM040_VAR['SUM_PAYABLE_1_12']-KRM040_VAR['SUM_REVOL_BAL_1_12']-KRM040_VAR['SUM_CASH_LENT_1_12']
            #循環>0序列
            KRM040_byPERIOD_1_12['REVOL_FLAG_1']=KRM040_byPERIOD_1_12['REVOL_FLAG'].astype(str).fillna('0')
            KRM040_VAR['REVOL_SEQ']=KRM040_byPERIOD_1_12.groupby(['CUSTOMER_ID'])['REVOL_FLAG_1'].apply(lambda x: "%s" % ''.join(x)).reset_index()['REVOL_FLAG_1']
            
            #PAYOCDE
            KRM040_byPERIOD_1_12['PAY_CODE_ADJ_1']=KRM040_byPERIOD_1_12['PAY_CODE_ADJ'].astype(str).fillna(' ')
            KRM040_VAR['PAY_CODE_ADJ_SEQ']=KRM040_byPERIOD_1_12.groupby(['CUSTOMER_ID'])['PAY_CODE_ADJ_1'].apply(lambda x: "%s" % ''.join(x)).reset_index()['PAY_CODE_ADJ_1']
            
            
            #當月/近n月最差繳款狀態
            KRM040_VAR['JCC4_1']=KRM040_VAR['PAY_CODE_ADJ_1']
            KRM040_VAR['JCC4_3']=KRM040_VAR['PAY_CODE_ADJ_1_3']
            KRM040_VAR['JCC4_6']=KRM040_VAR['PAY_CODE_ADJ_1_6']
            KRM040_VAR['JCC4_12']=KRM040_VAR['PAY_CODE_ADJ_1_12']
            #近n月M1+張數
            KRM040_VAR['JCC5_1']=KRM040_byPERIOD_1_1[KRM040_byPERIOD_1_1['PAY_CODE_ADJ']>0]['BILL_CNT']
            KRM040_VAR['JCC5_3']=KRM040_byPERIOD_1_3[KRM040_byPERIOD_1_3['PAY_CODE_ADJ']>0][['CUSTOMER_ID','BILL_CNT']].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            KRM040_VAR['JCC5_6']=KRM040_byPERIOD_1_6[KRM040_byPERIOD_1_6['PAY_CODE_ADJ']>0][['CUSTOMER_ID','BILL_CNT']].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            KRM040_VAR['JCC5_12']=KRM040_byPERIOD_1_12[KRM040_byPERIOD_1_12['PAY_CODE_ADJ']>0][['CUSTOMER_ID','BILL_CNT']].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            KRM040_VAR['JCC5_1']=KRM040_VAR['JCC5_1'].fillna(0)
            KRM040_VAR['JCC5_3']=KRM040_VAR['JCC5_3'].fillna(0)
            KRM040_VAR['JCC5_6']=KRM040_VAR['JCC5_6'].fillna(0)
            KRM040_VAR['JCC5_12']=KRM040_VAR['JCC5_12'].fillna(0)
            #近n月M1+月數
            KRM040_VAR['JCC6_3']=KRM040_byPERIOD_1_3[KRM040_byPERIOD_1_3['PAY_CODE_ADJ']>0][['CUSTOMER_ID','period_adj']].groupby('CUSTOMER_ID', as_index=False).agg({'period_adj':'count'})['period_adj']
            KRM040_VAR['JCC6_6']=KRM040_byPERIOD_1_6[KRM040_byPERIOD_1_6['PAY_CODE_ADJ']>0][['CUSTOMER_ID','period_adj']].groupby('CUSTOMER_ID', as_index=False).agg({'period_adj':'count'})['period_adj']
            KRM040_VAR['JCC6_12']=KRM040_byPERIOD_1_12[KRM040_byPERIOD_1_12['PAY_CODE_ADJ']>0][['CUSTOMER_ID','period_adj']].groupby('CUSTOMER_ID', as_index=False).agg({'period_adj':'count'})['period_adj']
            KRM040_VAR['JCC6_3']=KRM040_VAR['JCC6_3'].fillna(0)
            KRM040_VAR['JCC6_6']=KRM040_VAR['JCC6_6'].fillna(0)
            KRM040_VAR['JCC6_12']=KRM040_VAR['JCC6_12'].fillna(0)
            #近n月M1+月數
            KRM040_VAR['JCC7_12']=KRM040_byPERIOD_1_12[KRM040_byPERIOD_1_12['PAY_CODE_ADJ']>0][['CUSTOMER_ID','period_adj']].groupby('CUSTOMER_ID', as_index=False).agg({'period_adj':'min'})['period_adj']
            KRM040_VAR['JCC7_12']=KRM040_VAR['JCC7_12'].fillna(9999)
            #近n月M1+餘額(實際)月平均
            KRM040_VAR['JCC8_1']=KRM040_VAR['SUM_PAYABLE_M1P_1']
            KRM040_VAR['JCC8_3']=np.where(KRM040_VAR['PAYABLE_CNT_1_3']>0,KRM040_VAR['SUM_PAYABLE_M1P_1_3']/KRM040_VAR['PAYABLE_CNT_1_3'],0)
            KRM040_VAR['JCC8_6']=np.where(KRM040_VAR['PAYABLE_CNT_1_6']>0,KRM040_VAR['SUM_PAYABLE_M1P_1_6']/KRM040_VAR['PAYABLE_CNT_1_6'],0)
            KRM040_VAR['JCC8_12']=np.where(KRM040_VAR['PAYABLE_CNT_1_12']>0,KRM040_VAR['SUM_PAYABLE_M1P_1_12']/KRM040_VAR['PAYABLE_CNT_1_12'],0)
            #永久額度總合
            KRM040_VAR['JCC9_1']=KRM040_VAR['SUM_PERM_LIMIT_1']
            KRM040_VAR['JCC9_3']=KRM040_VAR['SUM_PERM_LIMIT_1_3']
            KRM040_VAR['JCC9_6']=KRM040_VAR['SUM_PERM_LIMIT_1_6']
            KRM040_VAR['JCC9_12']=KRM040_VAR['SUM_PERM_LIMIT_1_12']
            #永久額度平均
            KRM040_VAR['JCC10']=np.where(KRM040_byPERIOD_1_1['BILL_CNT']>0,KRM040_VAR['JCC9_1']/KRM040_byPERIOD_1_1['BILL_CNT'],0)
            #永久額度最大值
            KRM040_VAR['JCC11']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'MAX_PERM_LIMIT':'max'})['MAX_PERM_LIMIT']
            #永久額度最小值
            KRM040_VAR['JCC12']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'MIN_PERM_LIMIT':'min'})['MIN_PERM_LIMIT']
            #永久額度<=5萬張數
            KRM040_VAR['JCC13']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'PERM_LIMIT_UNDER_5':'sum'})['PERM_LIMIT_UNDER_5']
            #永久額度>=30萬張數
            KRM040_VAR['JCC14']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'PERM_LIMIT_OVER_30':'sum'})['PERM_LIMIT_OVER_30']
            #永久額度>=50萬張數
            KRM040_VAR['JCC15']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'PERM_LIMIT_OVER_50':'sum'})['PERM_LIMIT_OVER_50']
            #臨時額度最大值
            KRM040_VAR['JCC16']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'MAX_TEMP_LIMIT':'max'})['MAX_TEMP_LIMIT']
            #最大調額幅度=(臨時額度-永久額度)最大值
            KRM040_VAR['JCC17']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'MAX_ADJ_LIMIT':'max'})['MAX_ADJ_LIMIT']
            #餘額(實際)月平均 餘額加總/帳單餘額>0月數
            KRM040_VAR['JCC18_1']=KRM040_VAR['SUM_PAYABLE_1']
            KRM040_VAR['JCC18_3']=np.where(KRM040_VAR['PAYABLE_CNT_1_3']>0,KRM040_VAR['SUM_PAYABLE_1_3']/KRM040_VAR['PAYABLE_CNT_1_3'],0)
            KRM040_VAR['JCC18_6']=np.where(KRM040_VAR['PAYABLE_CNT_1_6']>0,KRM040_VAR['SUM_PAYABLE_1_6']/KRM040_VAR['PAYABLE_CNT_1_6'],0)
            KRM040_VAR['JCC18_12']=np.where(KRM040_VAR['PAYABLE_CNT_1_12']>0,KRM040_VAR['SUM_PAYABLE_1_12']/KRM040_VAR['PAYABLE_CNT_1_12'],0)
            #餘額月平均 餘額加總/有帳單月數
            KRM040_VAR['JCC19_1']=KRM040_VAR['SUM_PAYABLE_1']
            KRM040_VAR['JCC19_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_PAYABLE_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC19_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_PAYABLE_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC19_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_PAYABLE_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #餘額最大/月
            KRM040_VAR['JCC20']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'MAX_PAYABLE':'max'})['MAX_PAYABLE']
            #餘額>0月數
            KRM040_VAR['JCC21_3']=KRM040_VAR['PAYABLE_CNT_1_3']
            KRM040_VAR['JCC21_6']=KRM040_VAR['PAYABLE_CNT_1_6']
            KRM040_VAR['JCC21_12']=KRM040_VAR['PAYABLE_CNT_1_12']
            #餘額>0月數比例
            KRM040_VAR['JCC22_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['PAYABLE_CNT_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC22_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['PAYABLE_CNT_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC22_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['PAYABLE_CNT_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #餘額平均使用率
            KRM040_VAR['JCC25_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1']>0,KRM040_VAR['JCC19_1']==0,KRM040_VAR['JCC19_1']>0],[KRM040_VAR['JCC19_1']/KRM040_VAR['SUM_PERM_LIMIT_1'],0,9999],default=-9999)
            KRM040_VAR['JCC25_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_3']>0,KRM040_VAR['JCC19_3']==0,KRM040_VAR['JCC19_3']>0],[KRM040_VAR['JCC19_3']/KRM040_VAR['SUM_PERM_LIMIT_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC25_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_6']>0,KRM040_VAR['JCC19_6']==0,KRM040_VAR['JCC19_6']>0],[KRM040_VAR['JCC19_6']/KRM040_VAR['SUM_PERM_LIMIT_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC25_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_12']>0,KRM040_VAR['JCC19_12']==0,KRM040_VAR['JCC19_12']>0],[KRM040_VAR['JCC19_12']/KRM040_VAR['SUM_PERM_LIMIT_1_12'],0,9999],default=-9999)
            #餘額(+分期)>0月數
            KRM040_VAR['JCC26_3']=KRM040_VAR['PAYABLE_PREOWED_CNT_1_3']
            KRM040_VAR['JCC26_6']=KRM040_VAR['PAYABLE_PREOWED_CNT_1_6']
            KRM040_VAR['JCC26_12']=KRM040_VAR['PAYABLE_PREOWED_CNT_1_12']
            #餘額(+分期)>0月數比例
            KRM040_VAR['JCC27_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['PAYABLE_PREOWED_CNT_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC27_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['PAYABLE_PREOWED_CNT_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC27_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['PAYABLE_PREOWED_CNT_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #月平均餘額
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1']=np.where(KRM040_VAR['PERIOD_CNT_1']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_1']/KRM040_VAR['PERIOD_CNT_1'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_2']=np.where(KRM040_VAR['PERIOD_CNT_2']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_2']/KRM040_VAR['PERIOD_CNT_2'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_4_6']=np.where(KRM040_VAR['PERIOD_CNT_4_6']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_4_6']/KRM040_VAR['PERIOD_CNT_4_6'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_7_12']=np.where(KRM040_VAR['PERIOD_CNT_7_12']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_7_12']/KRM040_VAR['PERIOD_CNT_7_12'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #餘額(+分期)平均使用率
            KRM040_VAR['JCC28_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1']/KRM040_VAR['SUM_PERM_LIMIT_1'],0,9999],default=-9999)
            KRM040_VAR['AVG_USAGE_2']=np.select([KRM040_VAR['SUM_PERM_LIMIT_2']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_2']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_2']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_2']/KRM040_VAR['SUM_PERM_LIMIT_2'],0,9999],default=-9999)
            KRM040_VAR['JCC28_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_3']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_3']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_3']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_3']/KRM040_VAR['SUM_PERM_LIMIT_1_3'],0,9999],default=-9999)
            KRM040_VAR['AVG_USAGE_4_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_4_6']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_4_6']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_4_6']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_4_6']/KRM040_VAR['SUM_PERM_LIMIT_4_6'],0,9999],default=-9999)
            KRM040_VAR['JCC28_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_6']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_6']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_6']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_6']/KRM040_VAR['SUM_PERM_LIMIT_1_6'],0,9999],default=-9999)
            KRM040_VAR['AVG_USAGE_7_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_7_12']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_7_12']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_7_12']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_7_12']/KRM040_VAR['SUM_PERM_LIMIT_7_12'],0,9999],default=-9999)
            KRM040_VAR['JCC28_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_12']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_12']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_12']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_1_12']/KRM040_VAR['SUM_PERM_LIMIT_1_12'],0,9999],default=-9999)
            #餘額(+分期)平均使用率變動率
            KRM040_VAR['JCC29_1']=np.select([KRM040_VAR['AVG_USAGE_2']>0,(KRM040_VAR['JCC28_1']-KRM040_VAR['AVG_USAGE_2'])==0,(KRM040_VAR['JCC28_1']-KRM040_VAR['AVG_USAGE_2'])>0],[(KRM040_VAR['JCC28_1']-KRM040_VAR['AVG_USAGE_2'])/KRM040_VAR['AVG_USAGE_2'],0,9999],default=-9999)
            KRM040_VAR['JCC29_3']=np.select([KRM040_VAR['AVG_USAGE_4_6']>0,(KRM040_VAR['JCC28_3']-KRM040_VAR['AVG_USAGE_4_6'])==0,(KRM040_VAR['JCC28_3']-KRM040_VAR['AVG_USAGE_4_6'])>0],[(KRM040_VAR['JCC28_3']-KRM040_VAR['AVG_USAGE_4_6'])/KRM040_VAR['AVG_USAGE_4_6'],0,9999],default=-9999)
            KRM040_VAR['JCC29_6']=np.select([KRM040_VAR['AVG_USAGE_7_12']>0,(KRM040_VAR['JCC28_6']-KRM040_VAR['AVG_USAGE_7_12'])==0,(KRM040_VAR['JCC28_6']-KRM040_VAR['AVG_USAGE_7_12'])>0],[(KRM040_VAR['JCC28_6']-KRM040_VAR['AVG_USAGE_7_12'])/KRM040_VAR['AVG_USAGE_7_12'],0,9999],default=-9999)
            #月平均餘額(非AE卡)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1']=np.where(KRM040_VAR['PERIOD_CNT_1']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1']/KRM040_VAR['PERIOD_CNT_1'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_2']=np.where(KRM040_VAR['PERIOD_CNT_2']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_2']/KRM040_VAR['PERIOD_CNT_2'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_4_6']=np.where(KRM040_VAR['PERIOD_CNT_4_6']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_4_6']/KRM040_VAR['PERIOD_CNT_4_6'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_7_12']=np.where(KRM040_VAR['PERIOD_CNT_7_12']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_7_12']/KRM040_VAR['PERIOD_CNT_7_12'],0)
            KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_PAYABLE_PREOWED_WO_A82_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #餘額(+分期)平均使用率(非AE卡)
            KRM040_VAR['JCC28_WO_A82_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1'],0,9999],default=-9999)
            KRM040_VAR['JCC28_WO_A82_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_3']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_3']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_3']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_3']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC28_WO_A82_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_6']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_6']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_6']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_6']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC28_WO_A82_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_12']>0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_12']==0,KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_12']>0],[KRM040_VAR['AVG_MON_PAYABLE_PREOWED_WO_A82_1_12']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_12'],0,9999],default=-9999)
            #餘額(+分期)成長總和
            KRM040_VAR['JCC30_1']=KRM040_VAR['SUM_PAYABLE_PREOWED_1']-KRM040_VAR['SUM_PAYABLE_PREOWED_2']
            KRM040_VAR['JCC30_3']=KRM040_VAR['SUM_PAYABLE_PREOWED_1_3']-KRM040_VAR['SUM_PAYABLE_PREOWED_4_6']
            KRM040_VAR['JCC30_6']=KRM040_VAR['SUM_PAYABLE_PREOWED_1_6']-KRM040_VAR['SUM_PAYABLE_PREOWED_7_12']
            #餘額(+分期)成長率
            KRM040_VAR['JCC31_1']=np.select([KRM040_VAR['SUM_PAYABLE_PREOWED_2']!=0,KRM040_VAR['JCC30_1']==0,KRM040_VAR['JCC30_1']>0],[KRM040_VAR['JCC30_1']/KRM040_VAR['SUM_PAYABLE_PREOWED_2'],0,9999],default=-9999)
            KRM040_VAR['JCC31_3']=np.select([KRM040_VAR['SUM_PAYABLE_PREOWED_4_6']!=0,KRM040_VAR['JCC30_3']==0,KRM040_VAR['JCC30_3']>0],[KRM040_VAR['JCC30_3']/KRM040_VAR['SUM_PAYABLE_PREOWED_4_6'],0,9999],default=-9999)
            KRM040_VAR['JCC31_6']=np.select([KRM040_VAR['SUM_PAYABLE_PREOWED_7_12']!=0,KRM040_VAR['JCC30_6']==0,KRM040_VAR['JCC30_6']>0],[KRM040_VAR['JCC30_6']/KRM040_VAR['SUM_PAYABLE_PREOWED_7_12'],0,9999],default=-9999)
            #餘額(+分期)使用率 張數
            KRM040_VAR['JCC32']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_30':'sum'})['USAGE_PAYABLE_PRE_OWED_30']
            KRM040_VAR['JCC33']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_50':'sum'})['USAGE_PAYABLE_PRE_OWED_50']
            KRM040_VAR['JCC34']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_60':'sum'})['USAGE_PAYABLE_PRE_OWED_60']
            KRM040_VAR['JCC35']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_70':'sum'})['USAGE_PAYABLE_PRE_OWED_70']
            #餘額(+分期)使用率 張數(非AE卡)
            KRM040_VAR['JCC32_WO_A82']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_30_WO_A82':'sum'})['USAGE_PAYABLE_PRE_OWED_30_WO_A82']
            KRM040_VAR['JCC33_WO_A82']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_50_WO_A82':'sum'})['USAGE_PAYABLE_PRE_OWED_50_WO_A82']
            KRM040_VAR['JCC34_WO_A82']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_60_WO_A82':'sum'})['USAGE_PAYABLE_PRE_OWED_60_WO_A82']
            KRM040_VAR['JCC35_WO_A82']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'USAGE_PAYABLE_PRE_OWED_70_WO_A82':'sum'})['USAGE_PAYABLE_PRE_OWED_70_WO_A82']
            #循環餘額(實際)月平均 餘額加總/帳單餘額>0月數
            KRM040_VAR['JCC36_1']=KRM040_VAR['SUM_REVOL_BAL_1']
            KRM040_VAR['JCC36_3']=np.where(KRM040_VAR['REVOL_BAL_CNT_1_3']>0,KRM040_VAR['SUM_REVOL_BAL_1_3']/KRM040_VAR['REVOL_BAL_CNT_1_3'],0)
            KRM040_VAR['JCC36_6']=np.where(KRM040_VAR['REVOL_BAL_CNT_1_6']>0,KRM040_VAR['SUM_REVOL_BAL_1_6']/KRM040_VAR['REVOL_BAL_CNT_1_6'],0)
            KRM040_VAR['JCC36_12']=np.where(KRM040_VAR['REVOL_BAL_CNT_1_12']>0,KRM040_VAR['SUM_REVOL_BAL_1_12']/KRM040_VAR['REVOL_BAL_CNT_1_12'],0)
            #循環餘額月平均 餘額加總/有帳單月數
            KRM040_VAR['JCC37_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_REVOL_BAL_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC37_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_REVOL_BAL_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC37_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_REVOL_BAL_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #循環餘額最大/月
            KRM040_VAR['JCC38']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'MAX_REVOL_BAL':'sum'})['MAX_REVOL_BAL']
            #循環餘額>0月數
            KRM040_VAR['JCC39_3']=KRM040_VAR['REVOL_BAL_CNT_1_3']
            KRM040_VAR['JCC39_6']=KRM040_VAR['REVOL_BAL_CNT_1_6']
            KRM040_VAR['JCC39_12']=KRM040_VAR['REVOL_BAL_CNT_1_12']
            #循環餘額>0月數比例
            KRM040_VAR['JCC40_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['REVOL_BAL_CNT_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC40_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['REVOL_BAL_CNT_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC40_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['REVOL_BAL_CNT_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #循環餘額>0連續最大月數
            KRM040_VAR['JCC41_3']=KRM040_VAR['REVOL_SEQ'].str[:3].str.replace('0','').apply(len)
            KRM040_VAR['JCC41_6']=KRM040_VAR['REVOL_SEQ'].str[:6].str.replace('0','').apply(len)
            KRM040_VAR['JCC41_12']=KRM040_VAR['REVOL_SEQ'].str.replace('0','').apply(len)
            #循環餘額(+分期)(實際)月平均 餘額加總/帳單餘額>0月數
            KRM040_VAR['JCC42_1']=KRM040_VAR['SUM_REVOL_BAL_1']+KRM040_VAR['SUM_PRE_OWED_1']
            KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2']=KRM040_VAR['SUM_REVOL_BAL_2']+KRM040_VAR['SUM_PRE_OWED_2']
            KRM040_VAR['JCC42_3']=np.where(KRM040_VAR['REVOL_PREOWED_CNT_1_3']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_3']/KRM040_VAR['REVOL_PREOWED_CNT_1_3'],0)
            KRM040_VAR['JCC42_6']=np.where(KRM040_VAR['REVOL_PREOWED_CNT_1_6']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_6']/KRM040_VAR['REVOL_PREOWED_CNT_1_6'],0)
            KRM040_VAR['JCC42_12']=np.where(KRM040_VAR['REVOL_PREOWED_CNT_1_12']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_12']/KRM040_VAR['REVOL_PREOWED_CNT_1_12'],0)
            #循環餘額(+分期)月平均 餘額加總/有帳單月數
            KRM040_VAR['JCC43_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6']=np.where(KRM040_VAR['PERIOD_CNT_4_6']>0,KRM040_VAR['SUM_REVOL_PREOWED_4_6']/KRM040_VAR['PERIOD_CNT_4_6'],0)
            KRM040_VAR['JCC43_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12']=np.where(KRM040_VAR['PERIOD_CNT_7_12']>0,KRM040_VAR['SUM_REVOL_PREOWED_7_12']/KRM040_VAR['PERIOD_CNT_7_12'],0)
            KRM040_VAR['JCC43_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_REVOL_PREOWED_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #循環餘額(+分期)平均使用率
            KRM040_VAR['JCC44_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1']>0,KRM040_VAR['JCC42_1']==0,KRM040_VAR['JCC42_1']>0],[KRM040_VAR['JCC42_1']/KRM040_VAR['SUM_PERM_LIMIT_1'],0,9999],default=-9999)
            KRM040_VAR['JCC44_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_3']>0,KRM040_VAR['JCC43_3']==0,KRM040_VAR['JCC43_3']>0],[KRM040_VAR['JCC43_3']/KRM040_VAR['SUM_PERM_LIMIT_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC44_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_6']>0,KRM040_VAR['JCC43_6']==0,KRM040_VAR['JCC43_6']>0],[KRM040_VAR['JCC43_6']/KRM040_VAR['SUM_PERM_LIMIT_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC44_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_12']>0,KRM040_VAR['JCC43_12']==0,KRM040_VAR['JCC43_12']>0],[KRM040_VAR['JCC43_12']/KRM040_VAR['SUM_PERM_LIMIT_1_12'],0,9999],default=-9999)
            #循環餘額(+分期)>0月數
            KRM040_VAR['JCC45_3']=KRM040_VAR['REVOL_PREOWED_CNT_1_3']
            KRM040_VAR['JCC45_6']=KRM040_VAR['REVOL_PREOWED_CNT_1_6']
            KRM040_VAR['JCC45_12']=KRM040_VAR['REVOL_PREOWED_CNT_1_12']
            #循環餘額(+分期)>0月數比例
            KRM040_VAR['JCC46_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['REVOL_PREOWED_CNT_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC46_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['REVOL_PREOWED_CNT_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC46_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['REVOL_PREOWED_CNT_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #循環餘額(+分期)平均使用率(非AE卡)
            KRM040_VAR['JCC47_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1']>0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1']==0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1']>0],[KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1'],0,9999],default=-9999)
            KRM040_VAR['JCC47_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_3']>0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_3']==0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_3']>0],[KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_3']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC47_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_6']>0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_6']==0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_6']>0],[KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_6']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC47_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_12']>0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_12']==0,KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_12']>0],[KRM040_VAR['SUM_REVOL_PREOWED_WO_A82_1_12']/KRM040_VAR['SUM_PERM_LIMIT_WO_A82_1_12'],0,9999],default=-9999)
            #循環餘額(+分期)平均使用率變動率
            KRM040_VAR['JCC48_1']=np.select([KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2']>0,(KRM040_VAR['JCC42_1']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2'])==0,(KRM040_VAR['JCC42_1']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2'])>0],[(KRM040_VAR['JCC42_1']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2'])/KRM040_VAR['AVG_REVOL_PREOWED_USAGE_2'],0,9999],default=-9999)
            KRM040_VAR['JCC48_3']=np.select([KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6']>0,(KRM040_VAR['JCC43_3']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6'])==0,(KRM040_VAR['JCC43_3']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6'])>0],[(KRM040_VAR['JCC43_3']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6'])/KRM040_VAR['AVG_REVOL_PREOWED_USAGE_4_6'],0,9999],default=-9999)
            KRM040_VAR['JCC48_6']=np.select([KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12']>0,(KRM040_VAR['JCC43_6']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12'])==0,(KRM040_VAR['JCC43_6']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12'])>0],[(KRM040_VAR['JCC43_6']-KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12'])/KRM040_VAR['AVG_REVOL_PREOWED_USAGE_7_12'],0,9999],default=-9999)
            #循環餘額(+分期)成長總和
            KRM040_VAR['JCC49_1']=KRM040_VAR['SUM_REVOL_PREOWED_1']-KRM040_VAR['SUM_REVOL_PREOWED_2']
            KRM040_VAR['JCC49_3']=KRM040_VAR['SUM_REVOL_PREOWED_1_3']-KRM040_VAR['SUM_REVOL_PREOWED_4_6']
            KRM040_VAR['JCC49_6']=KRM040_VAR['SUM_REVOL_PREOWED_1_6']-KRM040_VAR['SUM_REVOL_PREOWED_7_12']
            #循環餘額(+分期)成長率
            KRM040_VAR['JCC50_1']=np.select([KRM040_VAR['SUM_REVOL_PREOWED_2']!=0,KRM040_VAR['JCC49_1']==0,KRM040_VAR['JCC49_1']>0],[KRM040_VAR['JCC49_1']/KRM040_VAR['SUM_REVOL_PREOWED_2'],0,9999],default=-9999)
            KRM040_VAR['JCC50_3']=np.select([KRM040_VAR['SUM_REVOL_PREOWED_4_6']!=0,KRM040_VAR['JCC49_3']==0,KRM040_VAR['JCC49_3']>0],[KRM040_VAR['JCC49_3']/KRM040_VAR['SUM_REVOL_PREOWED_4_6'],0,9999],default=-9999)
            KRM040_VAR['JCC50_6']=np.select([KRM040_VAR['SUM_REVOL_PREOWED_7_12']!=0,KRM040_VAR['JCC49_6']==0,KRM040_VAR['JCC49_6']>0],[KRM040_VAR['JCC49_6']/KRM040_VAR['SUM_REVOL_PREOWED_7_12'],0,9999],default=-9999)
            #循環餘額(+分期)使用率>30%張數
            KRM040_VAR['JCC51']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_30']
            KRM040_VAR['JCC52']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_50']
            KRM040_VAR['JCC53']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_70']
            KRM040_VAR['JCC54']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_80']
            #循環餘額(+分期)使用率>30%張數
            KRM040_VAR['JCC51_WO_A82']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_30_WO_A82']
            KRM040_VAR['JCC52_WO_A82']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_50_WO_A82']
            KRM040_VAR['JCC53_WO_A82']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_70_WO_A82']
            KRM040_VAR['JCC54_WO_A82']=KRM040_byPERIOD_1_1['USAGE_REVOL_PRE_OWED_80_WO_A82']
            #消費金額>0月數
            KRM040_VAR['JCC55_1']=KRM040_VAR['PURCHASE_CNT_1']
            KRM040_VAR['JCC55_3']=KRM040_VAR['PURCHASE_CNT_1_3']
            KRM040_VAR['JCC55_6']=KRM040_VAR['PURCHASE_CNT_1_6']
            KRM040_VAR['JCC55_12']=KRM040_VAR['PURCHASE_CNT_1_12']
            #消費金額>0家數
            KRM040_VAR['JCC56_1']=KRM040_byPERIOD_1_1['HAS_PURCHASE_BANK_CNT']
            KRM040_VAR['JCC56_3']=KRM040_byPERIOD_1_3.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_PURCHASE_BANK_CNT':'max'})['HAS_PURCHASE_BANK_CNT']
            KRM040_VAR['JCC56_6']=KRM040_byPERIOD_1_6.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_PURCHASE_BANK_CNT':'max'})['HAS_PURCHASE_BANK_CNT']
            KRM040_VAR['JCC56_12']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_PURCHASE_BANK_CNT':'max'})['HAS_PURCHASE_BANK_CNT']
            #消費金額總和 PAYABLE - REVOL_BAL
            KRM040_VAR['JCC57_1']=KRM040_VAR['SUM_PURCHASE_1']
            KRM040_VAR['JCC57_3']=KRM040_VAR['SUM_PURCHASE_1_3']
            KRM040_VAR['JCC57_6']=KRM040_VAR['SUM_PURCHASE_1_6']
            KRM040_VAR['JCC57_12']=KRM040_VAR['SUM_PURCHASE_1_12']
            #消費金額月平均
            KRM040_VAR['JCC58_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_PURCHASE_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC58_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_PURCHASE_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC58_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_PURCHASE_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #消費金額淨總和 PAYABLE - CASH_LENT - REVOL_BAL
            KRM040_VAR['JCC59_1']=KRM040_VAR['SUM_NET_PURCHASE_1']
            KRM040_VAR['JCC59_3']=KRM040_VAR['SUM_NET_PURCHASE_1_3']
            KRM040_VAR['JCC59_6']=KRM040_VAR['SUM_NET_PURCHASE_1_6']
            KRM040_VAR['JCC59_12']=KRM040_VAR['SUM_NET_PURCHASE_1_12']
            #消費金額淨月平均
            KRM040_VAR['JCC60_3']=np.where(KRM040_VAR['PERIOD_CNT_1_3']>0,KRM040_VAR['SUM_NET_PURCHASE_1_3']/KRM040_VAR['PERIOD_CNT_1_3'],0)
            KRM040_VAR['JCC60_6']=np.where(KRM040_VAR['PERIOD_CNT_1_6']>0,KRM040_VAR['SUM_NET_PURCHASE_1_6']/KRM040_VAR['PERIOD_CNT_1_6'],0)
            KRM040_VAR['JCC60_12']=np.where(KRM040_VAR['PERIOD_CNT_1_12']>0,KRM040_VAR['SUM_NET_PURCHASE_1_12']/KRM040_VAR['PERIOD_CNT_1_12'],0)
            #分期金額(實際)月平均 餘額加總/帳單餘額>0月數
            KRM040_VAR['JCC61_1']=KRM040_VAR['SUM_PRE_OWED_1']
            KRM040_VAR['JCC61_3']=np.where(KRM040_VAR['PREOWED_CNT_1_3']>0,KRM040_VAR['SUM_PRE_OWED_1_3']/KRM040_VAR['PREOWED_CNT_1_3'],0)
            KRM040_VAR['JCC61_6']=np.where(KRM040_VAR['PREOWED_CNT_1_6']>0,KRM040_VAR['SUM_PRE_OWED_1_6']/KRM040_VAR['PREOWED_CNT_1_6'],0)
            KRM040_VAR['JCC61_12']=np.where(KRM040_VAR['PREOWED_CNT_1_12']>0,KRM040_VAR['SUM_PRE_OWED_1_12']/KRM040_VAR['PREOWED_CNT_1_12'],0)
            #分期金額>0張數
            KRM040_VAR['JCC62']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'PRE_OWED_CNT':'sum'})['PRE_OWED_CNT']
            #分期金額使用率
            KRM040_VAR['JCC63_1']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1']!=0,KRM040_VAR['SUM_PRE_OWED_1']==0,KRM040_VAR['SUM_PRE_OWED_1']>0],[KRM040_VAR['SUM_PRE_OWED_1']/KRM040_VAR['SUM_PERM_LIMIT_1'],0,9999],default=-9999)
            KRM040_VAR['JCC63_3']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_3']!=0,KRM040_VAR['SUM_PRE_OWED_1_3']==0,KRM040_VAR['SUM_PRE_OWED_1_3']>0],[KRM040_VAR['SUM_PRE_OWED_1_3']/KRM040_VAR['SUM_PERM_LIMIT_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC63_6']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_6']!=0,KRM040_VAR['SUM_PRE_OWED_1_6']==0,KRM040_VAR['SUM_PRE_OWED_1_6']>0],[KRM040_VAR['SUM_PRE_OWED_1_6']/KRM040_VAR['SUM_PERM_LIMIT_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC63_12']=np.select([KRM040_VAR['SUM_PERM_LIMIT_1_12']!=0,KRM040_VAR['SUM_PRE_OWED_1_12']==0,KRM040_VAR['SUM_PRE_OWED_1_12']>0],[KRM040_VAR['SUM_PRE_OWED_1_12']/KRM040_VAR['SUM_PERM_LIMIT_1_12'],0,9999],default=-9999)
            #分期金額/餘額平均佔比
            KRM040_VAR['JCC64_1']=np.select([KRM040_VAR['SUM_PAYABLE_1']!=0,KRM040_VAR['SUM_PRE_OWED_1']==0,KRM040_VAR['SUM_PRE_OWED_1']>0],[KRM040_VAR['SUM_PRE_OWED_1']/KRM040_VAR['SUM_PAYABLE_1'],0,9999],default=-9999)
            KRM040_VAR['JCC64_3']=np.select([KRM040_VAR['SUM_PAYABLE_1_3']!=0,KRM040_VAR['SUM_PRE_OWED_1_3']==0,KRM040_VAR['SUM_PRE_OWED_1_3']>0],[KRM040_VAR['SUM_PRE_OWED_1_3']/KRM040_VAR['SUM_PAYABLE_1_3'],0,9999],default=-9999)
            KRM040_VAR['JCC64_6']=np.select([KRM040_VAR['SUM_PAYABLE_1_6']!=0,KRM040_VAR['SUM_PRE_OWED_1_6']==0,KRM040_VAR['SUM_PRE_OWED_1_6']>0],[KRM040_VAR['SUM_PRE_OWED_1_6']/KRM040_VAR['SUM_PAYABLE_1_6'],0,9999],default=-9999)
            KRM040_VAR['JCC64_12']=np.select([KRM040_VAR['SUM_PAYABLE_1_12']!=0,KRM040_VAR['SUM_PRE_OWED_1_12']==0,KRM040_VAR['SUM_PRE_OWED_1_12']>0],[KRM040_VAR['SUM_PRE_OWED_1_12']/KRM040_VAR['SUM_PAYABLE_1_12'],0,9999],default=-9999)
            #預借現金餘額>0月數
            KRM040_VAR['JCC65_1']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'CASH_LENT_FLAG':'sum'})['CASH_LENT_FLAG']
            KRM040_VAR['JCC65_3']=KRM040_byPERIOD_1_3.groupby('CUSTOMER_ID', as_index=False).agg({'CASH_LENT_FLAG':'sum'})['CASH_LENT_FLAG']
            KRM040_VAR['JCC65_6']=KRM040_byPERIOD_1_6.groupby('CUSTOMER_ID', as_index=False).agg({'CASH_LENT_FLAG':'sum'})['CASH_LENT_FLAG']
            KRM040_VAR['JCC65_12']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'CASH_LENT_FLAG':'sum'})['CASH_LENT_FLAG']
            #預借現金餘額>0家數
            KRM040_VAR['JCC66_1']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_CASH_LENTE_BANK_CNT':'sum'})['HAS_CASH_LENTE_BANK_CNT']
            KRM040_VAR['JCC66_3']=KRM040_byPERIOD_1_3.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_CASH_LENTE_BANK_CNT':'sum'})['HAS_CASH_LENTE_BANK_CNT']
            KRM040_VAR['JCC66_6']=KRM040_byPERIOD_1_6.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_CASH_LENTE_BANK_CNT':'sum'})['HAS_CASH_LENTE_BANK_CNT']
            KRM040_VAR['JCC66_12']=KRM040_byPERIOD_1_12.groupby('CUSTOMER_ID', as_index=False).agg({'HAS_CASH_LENTE_BANK_CNT':'sum'})['HAS_CASH_LENTE_BANK_CNT']
            
            #預借現金餘額(實際)月平均 預借現金餘額/帳單餘額>0月數
            KRM040_VAR['JCC67_1']=KRM040_VAR['SUM_CASH_LENT_1']
            KRM040_VAR['JCC67_3']=np.where(KRM040_VAR['JCC65_3']>0,KRM040_VAR['SUM_CASH_LENT_1_3']/KRM040_VAR['JCC65_3'],0)
            KRM040_VAR['JCC67_6']=np.where(KRM040_VAR['JCC65_6']>0,KRM040_VAR['SUM_CASH_LENT_1_6']/KRM040_VAR['JCC65_6'],0)
            KRM040_VAR['JCC67_12']=np.where(KRM040_VAR['JCC65_12']>0,KRM040_VAR['SUM_CASH_LENT_1_12']/KRM040_VAR['JCC65_12'],0)
            #預借現金餘額月平均 預借現金餘額/有帳單月數
            KRM040_VAR['JCC68_3']=np.where(KRM040_VAR['PAYABLE_CNT_1_3']>0,KRM040_VAR['SUM_CASH_LENT_1_3']/KRM040_VAR['PAYABLE_CNT_1_3'],0)
            KRM040_VAR['JCC68_6']=np.where(KRM040_VAR['PAYABLE_CNT_1_6']>0,KRM040_VAR['SUM_CASH_LENT_1_6']/KRM040_VAR['PAYABLE_CNT_1_6'],0)
            KRM040_VAR['JCC68_12']=np.where(KRM040_VAR['PAYABLE_CNT_1_12']>0,KRM040_VAR['SUM_CASH_LENT_1_12']/KRM040_VAR['PAYABLE_CNT_1_12'],0)
            #近n期新增信卡張數
            KRM040_VAR['JCC69_3']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==3].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            KRM040_VAR['JCC69_6']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==6].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            KRM040_VAR['JCC69_12']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==12].groupby('CUSTOMER_ID', as_index=False).agg({'BILL_CNT':'sum'})['BILL_CNT']
            #近n期新增信用卡平均額度(取最近一期額度計算)
            KRM040_VAR['JCC70_3']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==3].groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']
            KRM040_VAR['JCC70_6']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==6].groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']
            KRM040_VAR['JCC70_12']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']-KRM040_byPERIOD.loc[KRM040_byPERIOD['period_adj']==12].groupby('CUSTOMER_ID', as_index=False).agg({'AVG_PERM_LIMIT':'sum'})['AVG_PERM_LIMIT']
            
            
            # ##### KRM046
            KRM046=KRM046.rename(
                columns={
                        'CARD_BRAND':'ISSUE_NAME',
                        'SYSDATE':'QUERY_DATE',
                        'IDN_BAN':'CUSTOMER_ID'
                    }
                )
            
            KRM046['LIMIT']=KRM046['LIMIT']*1000
            KRM046['QUERY_DATE']=pd.to_datetime(KRM046['QUERY_DATE'])
            
            #KRM040_VAR['JCC62']=KRM040_byPERIOD_1_1.groupby('CUSTOMER_ID', as_index=False).agg({'PRE_OWED_CNT':'sum'})['PRE_OWED_CNT']
            KRM046_VAR=KRM046.groupby(['CUSTOMER_ID','QUERY_DATE'], as_index=False).agg({'START_DATE':'min'})
            KRM046_VAR=KRM046_VAR.rename(columns={'START_DATE':'MIN_START_DATE'})
            KRM046_VAR['MIN_START_DATE']=pd.to_datetime(KRM046_VAR['MIN_START_DATE'])
            KRM046_VAR['JCC1']=((KRM046_VAR['QUERY_DATE'] - KRM046_VAR['MIN_START_DATE'])/np.timedelta64(1, 'M'))
            KRM046_VAR['MAX_START_DATE']=pd.to_datetime(KRM046.groupby(['CUSTOMER_ID','QUERY_DATE'], as_index=False).agg({'START_DATE':'max'})['START_DATE'])
            KRM046_VAR['JCC2']=((KRM046_VAR['QUERY_DATE'] - KRM046_VAR['MAX_START_DATE'])/np.timedelta64(1, 'M'))
            
            
            # ##### BAM028
            BAM028=BAM028.rename(
                columns={
                        'IDN_BAN':'CUSTOMER_ID',
                        'SYSDATE':'QUERY_DATE',
                        'BANK_CODE':'BANK'
                    }
                )
            
            
            BAM028['QUERY_DATE']=pd.to_datetime(BAM028['QUERY_DATE'])
            BAM028['BANK']=BAM028['BANK'].str[:3]
            BAM028_base=BAM028[['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE']].drop_duplicates()
            
            BAM028_VAR=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})
            BAM028_VAR=BAM028_VAR.rename(columns={'APPLICATION_NBR':'B28_BANK_NEW_LOAN_3'})
            BAM028_VAR['RM016']=np.where(BAM028_VAR['B28_BANK_NEW_LOAN_3']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_LOAN_6']=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM017']=np.where(BAM028_VAR['B28_BANK_NEW_LOAN_6']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_LOAN_12']=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM018']=np.where(BAM028_VAR['B28_BANK_NEW_LOAN_12']>0,1,0)
            BAM028_VAR['RM073']=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM074']=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM075']=BAM028[BAM028['NEW_LOAN_AMT']>0].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['B28_BANK_NEW_US_3']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM028']=np.where(BAM028_VAR['B28_BANK_NEW_US_3']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_US_6']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM029']=np.where(BAM028_VAR['B28_BANK_NEW_US_6']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_US_12']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM030']=np.where(BAM028_VAR['B28_BANK_NEW_US_12']>0,1,0)
            BAM028_VAR['RM079']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM080']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM08q']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & (BAM028['BANK']!='013') & ~BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['B28_BANK_NEW_S_3']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM040']=np.where(BAM028_VAR['B28_BANK_NEW_S_3']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_S_6']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM041']=np.where(BAM028_VAR['B28_BANK_NEW_S_6']>0,1,0)
            BAM028_VAR['B28_BANK_NEW_S_12']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM042']=np.where(BAM028_VAR['B28_BANK_NEW_S_12']>0,1,0)
            BAM028_VAR['RM085']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM086']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            BAM028_VAR['RM087']=BAM028[(BAM028['NEW_LOAN_AMT']>0) & BAM028['ACCOUNT_CODE2'].isin(['S','W','M'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'APPLICATION_NBR':'count'})['APPLICATION_NBR']
            
            
            # ##### STM007
            STM007=STM007.rename(
                columns={
                        'IDN_BAN':'CUSTOMER_ID',
                        'SYSDATE':'QUERY_DATE'
                    }
                )
            STM007['QUERY_DATE']=pd.to_datetime(STM007['QUERY_DATE'])
            STM007['BANK']=STM007['BANK_CODE'].str[:3]
            STM007['INQ_PURPOSE']=STM007['INQ_PURPOSE_1'].str.strip()
            
            STM007['NUM_INQ3']=np.where(STM007['BANK'].isna(),1,0)
            STM007['NUM_INQ3_NE_013']=np.where(STM007['BANK']!='013',1,0)
            STM007['NUM_INQ_NEW3']=np.where((STM007['BANK'].isna()) & (STM007['INQ_PURPOSE'].isin(['1','3','5','7'])),1,0)
            STM007['NUM_INQ_NEW3_NE_013']=np.where((STM007['BANK']!='013') & (STM007['INQ_PURPOSE'].isin(['1','3','5','7'])),1,0)
            STM007['NUM_INQ_OLD3']=np.where((STM007['BANK'].isna()) & (STM007['INQ_PURPOSE']=='2'),1,0)
            STM007['NUM_INQ_OLD3_NE_013']=np.where((STM007['BANK']!='013') & (STM007['INQ_PURPOSE']=='2'),1,0)
            
            STM007['BANK_INQ3']=np.where(STM007['BANK'].isna(),1,0)
            STM007['BANK_INQ3_NE_013']=np.where(STM007['BANK']!='013',1,0)
            STM007['BANK_INQ_NEW3']=np.where((STM007['BANK'].isna()) & (STM007['INQ_PURPOSE'].isin(['1','3','5','7'])),1,0)
            STM007['BANK_INQ_NEW3_NE_013']=np.where((STM007['BANK']!='013') & (STM007['INQ_PURPOSE'].isin(['1','3','5','7'])),1,0)
            STM007['BANK_INQ_OLD3']=np.where((STM007['BANK'].isna()) & (STM007['INQ_PURPOSE']=='2'),1,0)
            STM007['BANK_INQ_OLD3_NE_013']=np.where((STM007['BANK']!='013') & (STM007['INQ_PURPOSE']=='2'),1,0)
            
            STM007_groupby_IDBank=STM007.groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE','BANK'], as_index=False).agg({'NUM_INQ3':'sum',
                                                                                                                              'NUM_INQ3_NE_013':'sum',
                                                                                                                              'NUM_INQ_NEW3':'sum',
                                                                                                                              'NUM_INQ_NEW3_NE_013':'sum',
                                                                                                                              'NUM_INQ_OLD3':'sum',
                                                                                                                              'NUM_INQ_OLD3_NE_013':'sum',
                                                                                                                              'BANK_INQ3':'max',
                                                                                                                              'BANK_INQ3_NE_013':'max',
                                                                                                                              'BANK_INQ_NEW3':'max',
                                                                                                                              'BANK_INQ_NEW3_NE_013':'max',
                                                                                                                              'BANK_INQ_OLD3':'max',
                                                                                                                              'BANK_INQ_OLD3_NE_013':'max'
                                                                                                                             })
            STM007_groupby_IDBank['NUM_INQ3']=np.where(STM007_groupby_IDBank['BANK']=='013',1,STM007_groupby_IDBank['NUM_INQ3'])
            STM007_groupby_IDBank['NUM_INQ_NEW3']=np.select([(STM007_groupby_IDBank['BANK']=='013') & (STM007_groupby_IDBank['NUM_INQ_NEW3']>0),(STM007_groupby_IDBank['BANK']=='013') & (STM007_groupby_IDBank['NUM_INQ_NEW3']==0)],[1,0],default=STM007_groupby_IDBank['NUM_INQ_NEW3'])
            STM007_groupby_IDBank['NUM_INQ_OLD3']=np.select([(STM007_groupby_IDBank['BANK']=='013') & (STM007_groupby_IDBank['NUM_INQ_OLD3']>0),(STM007_groupby_IDBank['BANK']=='013') & (STM007_groupby_IDBank['NUM_INQ_OLD3']==0)],[1,0],default=STM007_groupby_IDBank['NUM_INQ_OLD3'])
            
            STM007_VAR=STM007_groupby_IDBank.groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'NUM_INQ3':'sum',
                                                                                                                              'NUM_INQ3_NE_013':'sum',
                                                                                                                              'NUM_INQ_NEW3':'sum',
                                                                                                                              'NUM_INQ_NEW3_NE_013':'sum',
                                                                                                                              'NUM_INQ_OLD3':'sum',
                                                                                                                              'NUM_INQ_OLD3_NE_013':'sum',
                                                                                                                              'BANK_INQ3':'max',
                                                                                                                              'BANK_INQ3_NE_013':'max',
                                                                                                                              'BANK_INQ_NEW3':'max',
                                                                                                                              'BANK_INQ_NEW3_NE_013':'max',
                                                                                                                              'BANK_INQ_OLD3':'max',
                                                                                                                              'BANK_INQ_OLD3_NE_013':'max'
                                                                                                                             })
            STM007_VAR=STM007_VAR.rename(columns={'NUM_INQ3':'RM179',
                                                  'NUM_INQ3_NE_013':'RM180',
                                                  'NUM_INQ_NEW3':'RM181',
                                                  'NUM_INQ_NEW3_NE_013':'RM182',
                                                  'NUM_INQ_OLD3':'RM183',
                                                  'NUM_INQ_OLD3_NE_013':'RM184',
                                                  'BANK_INQ3':'RM185',
                                                  'BANK_INQ3_NE_013':'RM186',
                                                  'BANK_INQ_NEW3':'RM187',
                                                  'BANK_INQ_NEW3_NE_013':'RM188',
                                                  'BANK_INQ_OLD3':'RM189',
                                                  'BANK_INQ_OLD3_NE_013':'RM190'
                                                 })
            
            # ##### VAM106 VAM107 VAM108
            VAM=VAM.rename(
                columns={
                        'IDN_BAN':'CUSTOMER_ID',
                        'MAINCODE':'MAIN_CODE',
                        'MAINNOTE':'MAIN_NOTE',
                        'NEW_SUBCODE':'NEW_SUB_CODE',
                        'SYSDATE':'QUERY_DATE',
                        'SUBCODE':'SUB_CODE',
                        'SUBNOTE':'SUB_NOTE'
                    }
                )
            VAM['SUB_CODE']=np.where((VAM['SUB_CODE'].str.strip()=='') | (VAM['SUB_CODE'].isna()),VAM['NEW_SUB_CODE'],VAM['SUB_CODE'])
            VAM['QUERY_DATE']=pd.to_datetime(VAM['QUERY_DATE'])
            #==' '#.str.len()
            
            VAM[VAM['MAIN_CODE'].isin(['7','8','A','B','C','E'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'MAIN_CODE':'count'})
            VAM_VAR=VAM[VAM['MAIN_CODE'].isin(['7','8','A','B','C','E'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'MAIN_CODE':'count'})
            VAM_VAR=VAM_VAR.rename(columns={'MAIN_CODE':'NEGO'})
            
            VAM_VAR['OTH_FLAG']=VAM[~VAM['MAIN_CODE'].isin(['7','8','A','B','C','E'])].groupby(['CUSTOMER_ID','APPLICATION_NBR','QUERY_DATE'], as_index=False).agg({'MAIN_CODE':'count'})['MAIN_CODE']
            
            
            # ##### JCIC vars combine
            BAM095_VAR['CUSTOMER_ID']=BAM095_VAR['CUSTOMER_ID'].str.strip()
            KRM040_VAR['CUSTOMER_ID']=KRM040_VAR['CUSTOMER_ID'].str.strip()
            KRM046_VAR['CUSTOMER_ID']=KRM046_VAR['CUSTOMER_ID'].str.strip()
            BAM028_VAR['CUSTOMER_ID']=BAM028_VAR['CUSTOMER_ID'].str.strip()
            STM007_VAR['CUSTOMER_ID']=STM007_VAR['CUSTOMER_ID'].str.strip()
            JCIC_VAR=application_data_df.merge(BAM095_VAR,how='outer',on='CUSTOMER_ID') .merge(KRM040_VAR,how='outer',on='CUSTOMER_ID') .merge(KRM046_VAR,how='outer',on='CUSTOMER_ID') .merge(BAM028_VAR,how='outer',on='CUSTOMER_ID') .merge(STM007_VAR,how='outer',on='CUSTOMER_ID')# JCIC_VAR['Rating_date']=Rating_date
            # JCIC_VAR['Rating_date']=pd.to_datetime(JCIC_VAR['Rating_date'])
            
            if len(BAM095_VAR)==0:
                JCIC_VAR['BAM095_FLAG']=0
            else: JCIC_VAR['BAM095_FLAG']=1
            if len(KRM040_VAR)==0:
                JCIC_VAR['KRM040_FLAG']=0
            else: JCIC_VAR['KRM040_FLAG']=1
            if len(KRM046_VAR)==0:
                JCIC_VAR['KRM046_FLAG']=0
            else: JCIC_VAR['KRM046_FLAG']=1
            JCIC_VAR=JCIC_VAR.rename(columns={'PAY_CODE_ADJ_SEQ_B':'BAM_PAYCODE12','PAY_CODE_ADJ_SEQ':'KRM_PAYCODE12'})
            #當月/近n月最差繳款狀態
            JCIC_VAR['CNC_JCC4_1']=np.where((JCIC_VAR['JCC4_1'].isna()) & (JCIC_VAR['CASHCARD_JCC4_1'].isna()),np.nan,JCIC_VAR[['JCC4_1', 'CASHCARD_JCC4_1']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC4_3']=np.where((JCIC_VAR['JCC4_3'].isna()) & (JCIC_VAR['CASHCARD_JCC4_3'].isna()),np.nan,JCIC_VAR[['JCC4_3', 'CASHCARD_JCC4_3']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC4_6']=np.where((JCIC_VAR['JCC4_6'].isna()) & (JCIC_VAR['CASHCARD_JCC4_6'].isna()),np.nan,JCIC_VAR[['JCC4_6', 'CASHCARD_JCC4_6']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC4_12']=np.where((JCIC_VAR['JCC4_12'].isna()) & (JCIC_VAR['CASHCARD_JCC4_12'].isna()),np.nan,JCIC_VAR[['JCC4_12', 'CASHCARD_JCC4_12']].fillna(0).values.max(1))
            #近n月M1+張數
            JCIC_VAR['CNC_JCC5_1']=np.where((JCIC_VAR['JCC5_1'].isna()) & (JCIC_VAR['CASHCARD_JCC5_1'].isna()),np.nan,JCIC_VAR[['JCC5_1', 'CASHCARD_JCC5_1']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC5_3']=np.where((JCIC_VAR['JCC5_3'].isna()) & (JCIC_VAR['CASHCARD_JCC5_3'].isna()),np.nan,JCIC_VAR[['JCC5_3', 'CASHCARD_JCC5_3']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC5_6']=np.where((JCIC_VAR['JCC5_6'].isna()) & (JCIC_VAR['CASHCARD_JCC5_6'].isna()),np.nan,JCIC_VAR[['JCC5_6', 'CASHCARD_JCC5_6']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC5_12']=np.where((JCIC_VAR['JCC5_12'].isna()) & (JCIC_VAR['CASHCARD_JCC5_12'].isna()),np.nan,JCIC_VAR[['JCC5_12', 'CASHCARD_JCC5_12']].fillna(0).values.max(1))
            #近n月M1+月數
            JCIC_VAR['CNC_PAYCODE_1']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_1ST'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_1ST_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_1ST', 'PAY_CODE_ADJ_DELQ_1ST_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_2']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_2ND'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_2ND_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_2ND', 'PAY_CODE_ADJ_DELQ_2ND_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_3']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_3RD'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_3RD_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_3RD', 'PAY_CODE_ADJ_DELQ_3RD_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_4']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_4TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_4TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_4TH', 'PAY_CODE_ADJ_DELQ_4TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_5']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_5TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_5TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_5TH', 'PAY_CODE_ADJ_DELQ_5TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_6']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_6TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_6TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_6TH', 'PAY_CODE_ADJ_DELQ_6TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_7']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_7TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_7TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_7TH', 'PAY_CODE_ADJ_DELQ_7TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_8']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_8TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_8TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_8TH', 'PAY_CODE_ADJ_DELQ_8TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_9']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_9TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_9TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_9TH', 'PAY_CODE_ADJ_DELQ_9TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_10']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_10TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_10TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_10TH', 'PAY_CODE_ADJ_DELQ_10TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_11']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_11TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_11TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_11TH', 'PAY_CODE_ADJ_DELQ_11TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            JCIC_VAR['CNC_PAYCODE_12']=np.where((JCIC_VAR['PAY_CODE_ADJ_DELQ_12TH'].isna()) & (JCIC_VAR['PAY_CODE_ADJ_DELQ_12TH_CASHCARD'].isna()),np.nan,JCIC_VAR[['PAY_CODE_ADJ_DELQ_12TH', 'PAY_CODE_ADJ_DELQ_12TH_CASHCARD']].fillna(0).astype(int).values.max(1))
            #近n月M1+張數
            JCIC_VAR['CNC_JCC6_3']=np.where((JCIC_VAR['CASHCARD_PAY_CODE_ADJ_DELQ'].isna()) & (JCIC_VAR['BAM_PAYCODE12'].isna()),np.nan,JCIC_VAR[['CNC_PAYCODE_1', 'CNC_PAYCODE_2','CNC_PAYCODE_3']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC6_6']=np.where((JCIC_VAR['CASHCARD_PAY_CODE_ADJ_DELQ'].isna()) & (JCIC_VAR['BAM_PAYCODE12'].isna()),np.nan,JCIC_VAR[['CNC_PAYCODE_1', 'CNC_PAYCODE_2','CNC_PAYCODE_3','CNC_PAYCODE_4', 'CNC_PAYCODE_5','CNC_PAYCODE_6']].fillna(0).values.max(1))
            JCIC_VAR['CNC_JCC6_12']=np.where((JCIC_VAR['CASHCARD_PAY_CODE_ADJ_DELQ'].isna()) & (JCIC_VAR['BAM_PAYCODE12'].isna()),np.nan,JCIC_VAR[['CNC_PAYCODE_1', 'CNC_PAYCODE_2','CNC_PAYCODE_3','CNC_PAYCODE_4', 'CNC_PAYCODE_5','CNC_PAYCODE_6','CNC_PAYCODE_7','CNC_PAYCODE_8','CNC_PAYCODE_9','CNC_PAYCODE_10','CNC_PAYCODE_11','CNC_PAYCODE_12']].fillna(0).values.max(1))
            #近一次M1+距今月數
            JCIC_VAR['CNC_JCC7_12']=np.select([JCIC_VAR['CNC_PAYCODE_1']>0,
                                               JCIC_VAR['CNC_PAYCODE_2']>0,
                                               JCIC_VAR['CNC_PAYCODE_3']>0,
                                               JCIC_VAR['CNC_PAYCODE_4']>0,
                                               JCIC_VAR['CNC_PAYCODE_5']>0,
                                               JCIC_VAR['CNC_PAYCODE_6']>0,
                                               JCIC_VAR['CNC_PAYCODE_7']>0,
                                               JCIC_VAR['CNC_PAYCODE_8']>0,
                                               JCIC_VAR['CNC_PAYCODE_9']>0,
                                               JCIC_VAR['CNC_PAYCODE_10']>0,
                                               JCIC_VAR['CNC_PAYCODE_11']>0,
                                               JCIC_VAR['CNC_PAYCODE_12']>0,
                                              ],[1,2,3,4,5,6,7,8,9,10,11,12])
            #當月M1+餘額
            JCIC_VAR['CNC_JCC8_1']=np.where((JCIC_VAR['JCC8_1'].isna()) & (JCIC_VAR['CASHCARD_JCC8_1'].isna()),np.nan,JCIC_VAR['JCC8_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC8_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_JCC8_1']=np.where((JCIC_VAR['JCC8_1'].isna()) & (JCIC_VAR['CASHCARD_JCC8_1'].isna()) & (JCIC_VAR['JLN13_4'].isna()),np.nan,JCIC_VAR['JCC8_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC8_1']+JCIC_VAR['JLN13_4'].fillna(0))
            #--永久額度總合
            JCIC_VAR['CNC_JCC9_1']=np.where((JCIC_VAR['JCC9_1'].isna()) & (JCIC_VAR['CASHCARD_JCC9_1'].isna()),np.nan,JCIC_VAR['JCC9_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC9_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_JCC9_1']=np.where((JCIC_VAR['JCC9_1'].isna()) & (JCIC_VAR['CASHCARD_JCC9_1'].isna()) & (JCIC_VAR['JLN16_4'].isna()),np.nan,JCIC_VAR['JCC9_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC9_1']+JCIC_VAR['JLN16_4'].fillna(0))
            JCIC_VAR['CNC_JCC9_WO_A82_1']=np.where((JCIC_VAR['SUM_PERM_LIMIT_WO_A82_1'].isna()) & (JCIC_VAR['CASHCARD_JCC9_1'].isna()),np.nan,JCIC_VAR['SUM_PERM_LIMIT_WO_A82_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC9_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_JCC9_WO_A82_1']=np.where((JCIC_VAR['SUM_PERM_LIMIT_WO_A82_1'].isna()) & (JCIC_VAR['CASHCARD_JCC9_1'].isna()) & (JCIC_VAR['JLN16_4'].isna()),np.nan,JCIC_VAR['SUM_PERM_LIMIT_WO_A82_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC9_1']+JCIC_VAR['JLN16_4'].fillna(0))
            #永久額度<=5萬張數
            JCIC_VAR['CNC_JCC13_1']=np.where((JCIC_VAR['JCC13'].isna()) & (JCIC_VAR['CASHCARD_JCC13_1'].isna()),np.nan,JCIC_VAR['JCC13'].fillna(0)+JCIC_VAR['CASHCARD_JCC13_1'].fillna(0))
            #永久額度>=30萬張數
            JCIC_VAR['CNC_JCC14_1']=np.where((JCIC_VAR['JCC14'].isna()) & (JCIC_VAR['CASHCARD_JCC14_1'].isna()),np.nan,JCIC_VAR['JCC14'].fillna(0)+JCIC_VAR['CASHCARD_JCC14_1'].fillna(0))
            #永久額度>=50萬張數
            JCIC_VAR['CNC_JCC15_1']=np.where((JCIC_VAR['JCC15'].isna()) & (JCIC_VAR['CASHCARD_JCC15_1'].isna()),np.nan,JCIC_VAR['JCC15'].fillna(0)+JCIC_VAR['CASHCARD_JCC15_1'].fillna(0))
            ##餘額
            JCIC_VAR['CNC_JCC18_1']=np.where((JCIC_VAR['JCC18_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()),np.nan,JCIC_VAR['JCC18_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_JCC18_1']=np.where((JCIC_VAR['JCC18_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()) & (JCIC_VAR['JLN14_4'].isna()),np.nan,JCIC_VAR['JCC18_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1']+JCIC_VAR['JLN14_4'].fillna(0))
            #餘額平均使用率
            JCIC_VAR['CNC_JCC25_1']=np.select([((JCIC_VAR['CNC_JCC18_1'].isna()) | (JCIC_VAR['CNC_JCC9_1'].isna())) , JCIC_VAR['CNC_JCC9_1']!=0,JCIC_VAR['CNC_JCC18_1']==0,JCIC_VAR['CNC_JCC18_1']>0],[np.nan , JCIC_VAR['CNC_JCC18_1']/JCIC_VAR['CNC_JCC9_1'],0,9999],default=-9999)
            JCIC_VAR['ALL_UNSEC_JCC25_1']=np.select([((JCIC_VAR['ALL_UNSEC_JCC18_1'].isna()) | (JCIC_VAR['ALL_UNSEC_JCC9_1'].isna())) , JCIC_VAR['ALL_UNSEC_JCC9_1']!=0,JCIC_VAR['ALL_UNSEC_JCC18_1']==0,JCIC_VAR['ALL_UNSEC_JCC18_1']>0],[np.nan , JCIC_VAR['ALL_UNSEC_JCC18_1']/JCIC_VAR['ALL_UNSEC_JCC9_1'],0,9999],default=-9999)
            #(循環+分期)(非AE卡)
            JCIC_VAR['CNC_REVOL_PREOWED_1']=np.where((JCIC_VAR['SUM_REVOL_PREOWED_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()),np.nan,JCIC_VAR['SUM_REVOL_PREOWED_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_1']=np.where((JCIC_VAR['SUM_REVOL_PREOWED_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()) & (JCIC_VAR['JLN14_4'].isna()),np.nan,JCIC_VAR['SUM_REVOL_PREOWED_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1']+JCIC_VAR['JLN14_4'].fillna(0))
            JCIC_VAR['CNC_PAYABLE_REVOL_WO_A82_1']=np.where((JCIC_VAR['SUM_REVOL_PREOWED_WO_A82_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()),np.nan,JCIC_VAR['SUM_REVOL_PREOWED_WO_A82_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1'].fillna(0))
            JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_WO_A82_1']=np.where((JCIC_VAR['SUM_REVOL_PREOWED_WO_A82_1'].isna()) & (JCIC_VAR['CASHCARD_JCC18_1'].isna()) & (JCIC_VAR['JLN14_4'].isna()),np.nan,JCIC_VAR['SUM_REVOL_PREOWED_WO_A82_1'].fillna(0)+JCIC_VAR['CASHCARD_JCC18_1']+JCIC_VAR['JLN14_4'].fillna(0))
            #循環餘額(+分期)平均使用率
            JCIC_VAR['CNC_JCC44_1']=np.select([((JCIC_VAR['CNC_REVOL_PREOWED_1'].isna()) | (JCIC_VAR['CNC_JCC9_1'].isna())) , JCIC_VAR['CNC_JCC9_1']!=0,JCIC_VAR['CNC_REVOL_PREOWED_1']==0,JCIC_VAR['CNC_REVOL_PREOWED_1']>0],[np.nan , JCIC_VAR['CNC_REVOL_PREOWED_1']/JCIC_VAR['CNC_JCC9_1'],0,9999],default=-9999)
            JCIC_VAR['ALL_UNSEC_JCC44_1']=np.select([((JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_1'].isna()) | (JCIC_VAR['ALL_UNSEC_JCC9_1'].isna())) , JCIC_VAR['ALL_UNSEC_JCC9_1']!=0,JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_1']==0,JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_1']>0],[np.nan , JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_1']/JCIC_VAR['ALL_UNSEC_JCC9_1'],0,9999],default=-9999)
            JCIC_VAR['CNC_JCC47_1']=np.select([((JCIC_VAR['CNC_PAYABLE_REVOL_WO_A82_1'].isna()) | (JCIC_VAR['CNC_JCC9_WO_A82_1'].isna())) , JCIC_VAR['CNC_JCC9_WO_A82_1']!=0,JCIC_VAR['CNC_PAYABLE_REVOL_WO_A82_1']==0,JCIC_VAR['CNC_PAYABLE_REVOL_WO_A82_1']>0],[np.nan , JCIC_VAR['CNC_PAYABLE_REVOL_WO_A82_1']/JCIC_VAR['CNC_JCC9_WO_A82_1'],0,9999],default=-9999)
            JCIC_VAR['ALL_UNSEC_JCC47_1']=np.select([((JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_WO_A82_1'].isna()) | (JCIC_VAR['ALL_UNSEC_JCC9_WO_A82_1'].isna())) , JCIC_VAR['ALL_UNSEC_JCC9_WO_A82_1']!=0,JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_WO_A82_1']==0,JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_WO_A82_1']>0],[np.nan , JCIC_VAR['ALL_UNSEC_REVOL_PREOWED_WO_A82_1']/JCIC_VAR['ALL_UNSEC_JCC9_WO_A82_1'],0,9999],default=-9999)
            #循環餘額(+分期)使用率>30%張數(非AE卡)
            JCIC_VAR['CNC_JCC51_1']=np.where((JCIC_VAR['JCC51'].isna()) & (JCIC_VAR['CASHCARD_JCC51_1'].isna()),np.nan,JCIC_VAR['JCC51'].fillna(0)+JCIC_VAR['CASHCARD_JCC51_1'].fillna(0))
            #循環餘額(+分期)使用率>50%張數(非AE卡)
            JCIC_VAR['CNC_JCC52_1']=np.where((JCIC_VAR['JCC52'].isna()) & (JCIC_VAR['CASHCARD_JCC52_1'].isna()),np.nan,JCIC_VAR['JCC52'].fillna(0)+JCIC_VAR['CASHCARD_JCC52_1'].fillna(0))
            #循環餘額(+分期)使用率>70%張數(非AE卡)
            JCIC_VAR['CNC_JCC53_1']=np.where((JCIC_VAR['JCC53'].isna()) & (JCIC_VAR['CASHCARD_JCC53_1'].isna()),np.nan,JCIC_VAR['JCC53'].fillna(0)+JCIC_VAR['CASHCARD_JCC53_1'].fillna(0))
            #循環餘額(+分期)使用率>80%張數(非AE卡)
            JCIC_VAR['CNC_JCC54_1']=np.where((JCIC_VAR['JCC54'].isna()) & (JCIC_VAR['CASHCARD_JCC54_1'].isna()),np.nan,JCIC_VAR['JCC54'].fillna(0)+JCIC_VAR['CASHCARD_JCC54_1'].fillna(0))
            
            VAM_VAR=VAM_VAR.rename(columns={'QUERY_DATE':'QUERY_DATE_VAM'})
            VAM_VAR_JOIN=application_data_df.merge(VAM_VAR,how='outer',on='CUSTOMER_ID')
            
            JCIC_VAR=JCIC_VAR.merge(                       VAM_VAR_JOIN.groupby(['CUSTOMER_ID','application_nbr','Rating_date'], as_index=False).agg({'NEGO':'max','OTH_FLAG':'max'}),                       how='outer',on='CUSTOMER_ID')
            JCIC_VAR=JCIC_VAR.rename(columns={'NEGO':'VM01','OTH_FLAG':'VM03'})
            
            JCIC_VAR['VM02']=JCIC_VAR['VM01']
            JCIC_VAR['VM04']=JCIC_VAR['VM03']
            
            if len(VAM_VAR)==0:
                JCIC_VAR['VAM_FLAG']=0
            else:
                JCIC_VAR['VAM_FLAG']=1
            
            JCIC_VAR=JCIC_VAR.rename(columns={'Rating_date_x':'Rating_date'})
            
            JCIC_VAR = JCIC_VAR[['CUSTOMER_ID','APPLICATION_NBR','Rating_date','BAM095_FLAG','KRM040_FLAG','KRM046_FLAG','VAM_FLAG','BAM_PAYCODE12','KRM_PAYCODE12',
                      'JLN2_1','JLN4_1','JLN5_1','JLN6_1','JLN7_1','JLN7_4','JLN7_9','JLN8_1','JLN8_4','JLN8_9','JLN9_1','JLN9_4','JLN9_9','JLN10_1','JLN10_4','JLN10_9','JLN11_1','JLN11_4','JLN11_9',
                      'JLN12_1','JLN12_4','JLN12_9','JLN13_1','JLN13_4','JLN13_9','JLN3_1','JLN3_4','JLN3_9','JLN14_1','JLN14_2','JLN14_3','JLN14_4','JLN14_5','JLN14_6','JLN14_7','JLN14_8',
                      'JLN14_9','JLN14_10','JLN14_11','JLN14_12','JLN14_13','JLN14_14','JLN14_15','JLN14_16','JLN14_17','JLN14_18','JLN14_19','JLN14_20','JLN15_2','JLN15_3','JLN15_4','JLN15_5',
                        'JLN15_6','JLN15_9','JLN15_10','JLN15_11','JLN15_12','JLN15_13','JLN15_15','JLN15_17','JLN15_18','JLN15_19','JLN15_20','JLN16_1','JLN16_2','JLN16_3','JLN16_4','JLN16_5',
                        'JLN16_6','JLN16_7','JLN16_8','JLN16_9','JLN16_10','JLN16_11','JLN16_12','JLN16_13','JLN16_14','JLN16_15','JLN16_16','JLN16_17','JLN16_18','JLN16_19','JLN16_20','JLN17_1',
                        'JLN17_2','JLN17_3','JLN17_4','JLN17_9','JLN17_12','JLN17_13','JLN17_15','JLN17_16','JLN17_17','JLN17_18','JLN17_19','JLN17_20','JLN18_1','JLN18_2','JLN18_3','JLN18_4',
                        'JLN18_5','JLN18_6','JLN18_9','JLN18_10','JLN18_11','JLN18_12','JLN18_13','JLN18_15','JLN19_1','JLN19_2','JLN19_3','JLN19_4','JLN19_5','JLN19_6','JLN19_7','JLN19_8',
                        'JLN19_9','JLN19_10','JLN19_11','JLN19_12','JLN19_13','JLN19_14','JLN19_15','JLN19_16','JLN19_17','JLN19_18','JLN22_1','JLN22_2','JLN22_3','JLN22_4','JLN22_5','JLN22_6',
                        'JLN22_7','JLN22_8','JLN22_9','JLN22_10','JLN22_11','JLN22_12','JLN22_13','JLN22_14','JLN22_15','JLN22_16','JLN22_17','JLN22_18','JLN23_1','JLN23_2','JLN23_3','JLN23_4',
                        'JLN23_5','JLN23_6','JLN23_7','JLN23_8','JLN23_9','JLN23_10','JLN23_11','JLN23_12','JLN23_13','JLN23_14','JLN23_15','JLN23_16','JLN23_17','JLN23_18','JLN26_1','JLN27_1',
                        'JLN26_2','JLN27_2','JLN26_3','JLN27_3','JLN26_4','JLN27_4','JLN26_5','JLN27_5','JLN26_6','JLN27_6','JLN26_7','JLN27_7','JLN26_8','JLN27_8','JLN26_9','JLN27_9','JLN26_10',
                        'JLN27_10','JLN26_11','JLN27_11','JLN26_12','JLN27_12','JLN26_13','JLN27_13','JLN26_14','JLN27_14','JLN26_15','JLN27_15','JLN26_16','JLN27_16','JLN26_17','JLN27_17',
                        'JLN26_18','JLN27_18','JLN26_21','JLN27_21',
                        'CASHCARD_JCC4_1','CASHCARD_JCC4_3','CASHCARD_JCC4_6','CASHCARD_JCC4_12','CASHCARD_JCC5_1','CASHCARD_JCC5_3','CASHCARD_JCC5_6','CASHCARD_JCC5_12',
                        'CASHCARD_JCC6_1','CASHCARD_JCC6_3','CASHCARD_JCC6_6','CASHCARD_JCC6_12','CASHCARD_JCC7_12','CASHCARD_JCC8_1','CASHCARD_JCC9_1','CASHCARD_JCC10_1',
                        'CASHCARD_JCC11_1','CASHCARD_JCC12_1','CASHCARD_JCC13_1','CASHCARD_JCC14_1','CASHCARD_JCC15_1','CASHCARD_JCC18_1','CASHCARD_JCC44_1','CASHCARD_JCC51_1',
                        'CASHCARD_JCC52_1','CASHCARD_JCC53_1','CASHCARD_JCC54_1',
                        'JCC1','JCC2','JCC4_1','JCC4_3','JCC4_6','JCC4_12','JCC5_1','JCC5_3','JCC5_6','JCC5_12','JCC6_3','JCC6_6','JCC6_12','JCC7_12','JCC8_3','JCC8_6','JCC8_12','JCC9_1','JCC9_3',
                        'JCC9_6','JCC9_12','JCC10','JCC11','JCC12','JCC13','JCC14','JCC15','JCC16','JCC17','JCC18_1','JCC18_3','JCC18_6','JCC18_12','JCC19_1','JCC19_3','JCC19_6','JCC19_12','JCC20',
                        'JCC21_3','JCC21_6','JCC21_12','JCC22_3','JCC22_6','JCC22_12','JCC25_1','JCC25_3','JCC25_6','JCC25_12','JCC26_3','JCC26_6','JCC26_12','JCC27_3','JCC27_6','JCC27_12',
                        'JCC28_1','JCC28_3','JCC28_6','JCC28_12','JCC29_1','JCC29_3','JCC29_6','JCC28_WO_A82_1','JCC28_WO_A82_3','JCC28_WO_A82_6','JCC28_WO_A82_12','JCC30_1','JCC30_3',
                        'JCC30_6','JCC31_1','JCC31_3','JCC31_6','JCC32','JCC33','JCC34','JCC35','JCC32_WO_A82','JCC33_WO_A82','JCC34_WO_A82','JCC35_WO_A82','JCC36_1','JCC36_3','JCC36_6',
                        'JCC36_12','JCC37_3','JCC37_6','JCC37_12','JCC38','JCC39_3','JCC39_6','JCC39_12','JCC40_3','JCC40_6','JCC40_12','JCC41_3','JCC41_6','JCC41_12','JCC42_1','JCC42_3','JCC42_6',
                        'JCC42_12','JCC43_3','JCC43_6','JCC43_12','JCC44_1','JCC44_3','JCC44_6','JCC44_12','JCC45_3','JCC45_6','JCC45_12','JCC46_3','JCC46_6','JCC46_12','JCC47_1','JCC47_3',
                        'JCC47_6','JCC47_12','JCC48_1','JCC48_3','JCC48_6','JCC49_1','JCC49_3','JCC49_6','JCC50_1','JCC50_3','JCC50_6','JCC51','JCC52','JCC53','JCC54','JCC51_WO_A82',
                        'JCC52_WO_A82','JCC53_WO_A82','JCC54_WO_A82','JCC55_1','JCC55_3','JCC55_6','JCC55_12','JCC56_1','JCC56_3','JCC56_6','JCC56_12','JCC57_1','JCC57_3','JCC57_6','JCC57_12',
                        'JCC58_3','JCC58_6','JCC58_12','JCC59_1','JCC59_3','JCC59_6','JCC59_12','JCC60_3','JCC60_6','JCC60_12','JCC61_1','JCC61_3','JCC61_6','JCC61_12','JCC62','JCC63_1','JCC63_3',
                        'JCC63_6','JCC63_12','JCC64_1','JCC64_3','JCC64_6','JCC64_12','JCC65_1','JCC65_3','JCC65_6','JCC65_12','JCC66_1','JCC66_3','JCC66_6','JCC66_12','JCC67_1','JCC67_3',
                        'JCC67_6','JCC67_12','JCC68_3','JCC68_6','JCC68_12','JCC69_3','JCC69_6','JCC69_12','JCC70_3','JCC70_6','JCC70_12',
                        'RM179','RM180','RM181','RM182','RM183','RM184','RM185','RM186','RM187','RM188','RM189','RM190',
                        'CNC_JCC4_1','CNC_JCC4_3','CNC_JCC4_6','CNC_JCC4_12','CNC_JCC5_1','CNC_JCC5_3','CNC_JCC5_6','CNC_JCC5_12','CNC_JCC6_3','CNC_JCC6_6','CNC_JCC6_12','CNC_JCC7_12',
                        'CNC_JCC8_1','CNC_JCC9_1','CNC_JCC9_WO_A82_1','CNC_JCC13_1','CNC_JCC14_1','CNC_JCC15_1','CNC_JCC18_1','CNC_JCC25_1','CNC_JCC44_1','CNC_JCC47_1','CNC_JCC51_1',
                          'CNC_JCC52_1','CNC_JCC53_1','CNC_JCC54_1',
                          'ALL_UNSEC_JCC8_1','ALL_UNSEC_JCC9_1','ALL_UNSEC_JCC9_WO_A82_1','ALL_UNSEC_JCC18_1','ALL_UNSEC_JCC25_1','ALL_UNSEC_JCC44_1','ALL_UNSEC_JCC47_1',
                        'VM01','VM02','VM03','VM04'
                    ]]
            
            JCIC_VAR.columns = map(str.lower, JCIC_VAR.columns)
            jcic_adv_data = JCIC_VAR[['customer_id', 'all_unsec_jcc44_1', 'all_unsec_jcc47_1', 'jcc37_12',
                   'cnc_jcc4_12', 'jcc7_12', 'jcc39_3', 'jcc8_6', 'jcc44_1', 'cnc_jcc47_1',
                   'cnc_jcc7_12', 'jcc65_6', 'jcc37_3', 'rm186', 'jcc44_12',
                   'jcc35_wo_a82', 'cnc_jcc6_12', 'jcc66_6', 'cnc_jcc44_1', 'jln18_4',
                   'jln27_4', 'jcc55_12', 'cnc_jcc54_1', 'jln2_1', 'jcc16', 'jcc68_6',
                   'jcc53_wo_a82', 'jcc28_wo_a82_12', 'jcc21_12', 'jln19_4', 'jcc4_3',
                   'jcc28_wo_a82_1', 'all_unsec_jcc25_1', 'cnc_jcc52_1', 'cnc_jcc53_1',
                   'cnc_jcc6_6', 'jcc1', 'jln16_5', 'jcc40_12', 'jln19_3', 'jln9_4',
                   'jln17_4', 'cnc_jcc51_1', 'cnc_jcc5_6', 'jcc8_3', 'all_unsec_jcc18_1',
                   'jcc25_12', 'cnc_jcc4_6', 'all_unsec_jcc8_1', 'jcc10', 'jln16_11',
                   'jln14_4', 'cnc_jcc5_12', 'jcc36_1', 'jcc50_6', 'jcc11', 'cnc_jcc8_1',
                   'jcc59_3', 'jcc63_6', 'all_unsec_jcc9_wo_a82_1', 'jcc19_12', 'jln18_9',
                   'jcc61_3', 'jln27_1', 'cnc_jcc9_1', 'cnc_jcc18_1', 'all_unsec_jcc9_1',
                   'cnc_jcc6_3', 'cnc_jcc25_1', 'jcc5_1', 'jcc2', 'cnc_jcc9_wo_a82_1',
                   'jln16_6', 'jln18_15', 'cnc_jcc4_3', 'cnc_jcc5_3']]
            
            
            # ### 	Step 6~Step8 (包成一包Try...except判斷)
            #
            
            # #### default分flag + 聯徵flag (後續預計從TD讀取)
            default_flags_all = pd.DataFrame({'CUSTOMER_ID': ['R279AB5CE4F2815079', 'A100D5CD0345DCA51E'],
                                              'CC_WO_FLAG_1M': [None, 1],
                                              'CC_SUSPEND_FLAG_1M': [None, None],
                                              'CC_NEGOTIATION_FLAG_1M': [0, None],
                                              'LN_WO_FLAG_1M': [None, 0],
                                              'LN_NEGOTIATION_FLAG_1M': [None, 0],
                                              'CC_M3P_FLAG_1M': [None, None],
                                              'LN_M3P_FLAG_1M': [None, 0],
                                              'FRAUD_FLAG': [None, 0],
            
                                              'CC_JCIC_M3P_FLAG_1M': [0, 0],
                                              'CC_JCIC_WO_FLAG_1M': [0, 0],
                                              'LN_JCIC_M3P_FLAG_1M': [0, 0],
                                              'LN_JCIC_WO_FLAG_1M': [0, 1],
                                              'BAS001_FLAG_1M': [0, 0],
                                              'KRS001_FLAG_1M': [0, 0],
                                              'DAS004_FLAG_1M': [0, 0],
                                              'VAM106107_FLAG_1M': [0, 0]
                                             })
            
            
            default_flags_all_trans = default_flags_all.fillna(0)
            default_flags_all_trans['flag_sum_all'] = default_flags_all_trans[default_flags_all_trans.columns].sum(axis=1)


            # TODO: Logging write into Oracle

            # TODO: 組成下行電文

            processed_data = application_data_df.to_json(orient="records", lines=True)
            # logger.info(processed_data)

            yield processed_data

        except Exception as e:
            logger.error(e)
            # logger.error(f"{event}")  # sink to another topic


if __name__ == "__main__":
    app.main()

