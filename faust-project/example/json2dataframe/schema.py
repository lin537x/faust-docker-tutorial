import pandas as pd

BAM095 = pd.DataFrame(
    [],
    columns=('ACCOUNT_CODE', 'ACCOUNT_CODE_FLAG', 'AC_PURE_AMT', 'APPLICATION_NBR',
       'BANK_CODE', 'CONTRACT_FLAG', 'CO_LOAN_FLAG', 'CURRENCY_CODE',
       'CUR_LIMIT_CODE', 'CUSTOMER_ID', 'DATA_MM', 'DATA_YYY', 'DB_SEQ_NBR',
       'Debt_SEC_DATA_MM', 'Debt_SEC_DATA_YYY', 'Debt_SEC_ORG_ID', 'FUND_FLAG',
       'FUND_IDV_ID', 'GUAR_KIND_CODE', 'OFF_LIMIT_FLAG', 'PASS_DUE_AMT',
       'PAY_12_CODE', 'PROJECT_CODE', 'PURPOSE_CODE', 'QUERY_DATE',
       'QUERY_LOG_ID', 'REVOLVING_FLAG', 'SNAPSHOT_DATE', 'SPLIT_LIMIT_AMT',
       'SUM_LIMIT_AMT', 'SYNDICATE_CODE', 'SYNDICATE_DATE',
       'SYNDICATE_LOAN_PERCENT', 'TOP_LIMIT_CODE', 'TOP_LIMIT_COM_ID',
       'UNDUE_LOAN_AMT', 'Y_ACCT_LIMIT_AMT', 'Y_ACCT_LIMIT_FLAG')
)


KRM040 = pd.DataFrame(
    [],
    columns = ('APPLICATION_NBR', 'BILL_DATE', 'BILL_TYPE_CODE', 'CARD_TYPE_CODE',
       'CASH_LENT_AMT', 'CASH_LIMIT_AMT', 'CUSTOMER_ID', 'DB_SEQ_NBR',
       'DEBT_CLEAR_DATE', 'DEBT_CLOSE_FLAG', 'DEBT_FLAG', 'ISSUE_BANK_CODE',
       'LAST_PAYABLE_AMT', 'LAST_PAY_AMT_STATUS_CODE',
       'LAST_PAY_TIME_STATUS_CODE', 'LAST_REVOL_BAL', 'LAST_REVOL_RATE',
       'PAYABLE_AMT', 'PERM_LIMIT_AMT', 'PRE_OWED_AMT', 'PRI_CUSTOMER_ID',
       'QUERY_DATE', 'QUERY_LOG_ID', 'SNAPSHOT_DATE', 'TEMP_LIMIT_AMT')
)


KRM046 = pd.DataFrame(
    [],
    columns = ('AMC_CLEAR_IND', 'APPLICATION_NBR', 'ARREARS_CLEAR_DATE',
       'BILL_TYPE_CODE', 'CARD_BRAND_CODE', 'CARD_TYPE_CODE', 'CLOSE_CODE',
       'CUSTOMER_ID', 'DB_SEQ_NBR', 'GUARANTOR_ID', 'ISSUE_BANK_CODE',
       'ISSUE_DATE', 'LIMIT_AMT', 'OPEN_FLAG', 'PHYSICAL_CARD_IND',
       'PRI_CUSTOMER_ID', 'QUERY_DATE', 'QUERY_LOG_ID', 'RELA_CODE',
       'RISK_AMT', 'SHARE_LIMIT_IND', 'SIGN_CODE', 'SNAPSHOT_DATE',
       'STOP_AB_CODE', 'STOP_CODE', 'STOP_DATE')
)


BAM028 = pd.DataFrame(
    [],
    columns = ('ACCOUNT_CODE', 'ACCOUNT_CODE2', 'APPLICATION_NBR', 'BANK_CODE',
       'BANK_NAME', 'CUSTOMER_ID', 'DB_SEQ_NBR', 'LASTMON_LOAN_AMT',
       'LASTMON_PASS_DUE_AMT', 'NEW_CONTRACT_AMT', 'NEW_LOAN_AMT',
       'NEW_PAY_LOAN_AMT', 'PAY_LOAN_DATE', 'QUERY_DATE', 'QUERY_LOG_ID',
       'SNAPSHOT_DATE')
)


STM007 = pd.DataFrame(
    [],
    columns = ('APPLICATION_NBR', 'BANK_CODE', 'BANK_NAME', 'CUSTOMER_ID', 'DATA_DATE',
       'DB_SEQ_NBR', 'INQ_PURPOSE', 'INQ_PURPOSE_1', 'ITEM_LIST', 'QUERY_DATE',
       'QUERY_LOG_ID', 'SNAPSHOT_DATE', 'BANK', 'NUM_INQ3', 'NUM_INQ3_NE_013',
       'NUM_INQ_NEW3', 'NUM_INQ_NEW3_NE_013', 'NUM_INQ_OLD3',
       'NUM_INQ_OLD3_NE_013', 'BANK_INQ3', 'BANK_INQ3_NE_013', 'BANK_INQ_NEW3',
       'BANK_INQ_NEW3_NE_013', 'BANK_INQ_OLD3', 'BANK_INQ_OLD3_NE_013')
)


STM007=STM007.rename(
    columns={
            'CUSTOMER_ID':'IDN_BAN',
            'ITEM_CODE_LIST':'ITEM_LIST',
            'QUERY_DATE':'SYSDATE'
        }
    )


VAM106 = pd.DataFrame(
    [],
    columns = ('APPLICATION_NBR', 'CUSTOMER_ID', 'DATA_DATE', 'DB_SEQ_NBR',
       'MAIN_CODE', 'MAIN_NOTE', 'NEW_SUB_CODE', 'NOTE', 'QUERY_DATE',
       'QUERY_LOG_ID', 'SNAPSHOT_DATE', 'SUB_CODE', 'SUB_NOTE')
)


VAM107 = pd.DataFrame(
    [],
    columns = ('APPLICATION_NBR', 'CUSTOMER_ID', 'DATA_DATE', 'DB_SEQ_NBR',
       'MAIN_CODE', 'MAIN_NOTE', 'NEW_SUB_CODE', 'NOTE', 'QUERY_DATE',
       'QUERY_LOG_ID', 'SNAPSHOT_DATE', 'SUB_CODE', 'SUB_NOTE')
)


VAM108 = pd.DataFrame(
    [],
    columns = ('APPLICATION_NBR', 'CUSTOMER_ID', 'DATA_DATE', 'DB_SEQ_NBR',
       'MAIN_CODE', 'MAIN_NOTE', 'NEW_SUB_CODE', 'NOTE', 'QUERY_DATE',
       'QUERY_LOG_ID', 'SNAPSHOT_DATE', 'SUB_CODE', 'SUB_NOTE')
)


def rename_jcic_atoms_df_columns(dict_):
    dict_["BAM095"]=dict_["BAM095"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'Debt_SEC_DATA_MM':'ASST_DATA_MM',
                'Debt_SEC_DATA_YYY':'ASST_DATA_YYY',
                'Debt_SEC_ORG_ID':'ASST_IDN_BAN',
                'FUND_FLAG':'IB_MARK',
                'FUND_IDV_ID':'IAB_BAN',
                'QUERY_DATE':'SYSDATE',
                'SYNDICATE_CODE':'UN_MARK',
                'SYNDICATE_DATE':'U_YYYMMDD',
                'SYNDICATE_LOAN_PERCENT':'U_RATE',
                'TOP_LIMIT_COM_ID':'CON_BAN'
            }
        )

    dict_["KRM040"]=dict_["KRM040"].rename(
        columns={
            'BILL_TYPE_CODE':'BILL_MARK',
            'CARD_TYPE_CODE':'CARD_TYPE',
            'CASH_LENT_AMT':'CASH_LENT',
            'CASH_LIMIT_AMT':'CASH_LIMIT',
            'CUSTOMER_ID':'IDN_BAN',
            'DEBT_CLEAR_DATE':'CLEAR_DATE',
            'DEBT_CLOSE_FLAG':'CLOSE_CODE',
            'DEBT_FLAG':'DEBT_CODE',
            'ISSUE_BANK_CODE':'ISSUE',
            'LAST_PAYABLE_AMT':'LAST_PAYA',
            'LAST_PAY_AMT_STATUS_CODE':'PAY_STAT',
            'LAST_PAY_TIME_STATUS_CODE':'PAY_CODE',
            'LAST_REVOL_BAL':'REVOL_BAL',
            'LAST_REVOL_RATE':'REVOL_RATE',
            'PAYABLE_AMT':'PAYABLE',
            'PERM_LIMIT_AMT':'PERM_LIMIT',
            'PRE_OWED_AMT':'PRE_OWED',
            'QUERY_DATE':'SYSDATE',
            'TEMP_LIMIT_AMT':'TEMP_LIMIT'
        }
    )

    dict_["KRM046"]=dict_["KRM046"].rename(
        columns={
                'AMC_CLEAR_IND':'YN_AMC_CLEAR',
                'ARREARS_CLEAR_DATE':'CLEAR_DATE',
                'BILL_TYPE_CODE':'BILL_MARK',
                'CARD_BRAND_CODE':'CARD_BRAND',
                'CARD_TYPE_CODE':'DISP_GROUP',
                'CUSTOMER_ID':'IDN_BAN',
                'GUARANTOR_ID':'GUAR_ID',
                'ISSUE_BANK_CODE':'ISSUE',
                'ISSUE_DATE':'START_DATE',
                'LIMIT_AMT':'LIMIT',
                'OPEN_FLAG':'ACT_MARK',
                'PHYSICAL_CARD_IND':'YN_PHYSICAL',
                'QUERY_DATE':'SYSDATE',
                'RELA_CODE':'RELA',
                'RISK_AMT':'RISK',
                'SHARE_LIMIT_IND':'YN_SHARE_LIMIT',
                'SIGN_CODE':'CARD_TYPE',
                'STOP_AB_CODE':'AB_CODE'
    
            }
        )
    
    dict_["BAM028"]=dict_["BAM028"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'LASTMON_PASS_DUE_AMT':'LASTMON_PASS_DUE_AMT',
                'QUERY_DATE':'SYSDATE',
                'BANK':'BANK_CODE'
            }
        )
    
    
    dict_["STM007"]=dict_["STM007"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'ITEM_CODE_LIST':'ITEM_LIST',
                'QUERY_DATE':'SYSDATE'
            }
        )
    
    dict_["VAM106"]=dict_["VAM106"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'MAIN_CODE':'MAINCODE',
                'MAIN_NOTE':'MAINNOTE',
                'NEW_SUB_CODE':'NEW_SUBCODE',
                'QUERY_DATE':'SYSDATE',
                'SUB_CODE':'SUBCODE',
                'SUB_NOTE':'SUBNOTE'
            }
        )
    
    dict_["VAM107"]=dict_["VAM107"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'MAIN_CODE':'MAINCODE',
                'MAIN_NOTE':'MAINNOTE',
                'NEW_SUB_CODE':'NEW_SUBCODE',
                'QUERY_DATE':'SYSDATE',
                'SUB_CODE':'SUBCODE',
                'SUB_NOTE':'SUBNOTE'
            }
        )
    
    dict_["VAM108"]=dict_["VAM108"].rename(
        columns={
                'CUSTOMER_ID':'IDN_BAN',
                'MAIN_CODE':'MAINCODE',
                'MAIN_NOTE':'MAINNOTE',
                'NEW_SUB_CODE':'NEW_SUBCODE',
                'QUERY_DATE':'SYSDATE',
                'SUB_CODE':'SUBCODE',
                'SUB_NOTE':'SUBNOTE'
            }
        )
    

Data_BCDEJKLNGHIMFA = pd.DataFrame({
    'customer_id': ['A123456678800009', 'A10430FBEB04D230D0', 'R279AB5CE4F2815079', 'A103BCF3D0342DB37E', 'A101C53BFF5A8504AC', 'A10185D48BEFBFC57A', 'A1012413E19BFC1CF6', 'A100DE816EF1BFA839', 'A100D5CD0345DCA51E', 'A100C8CEA23F1875BE'],
    'r3m_bal50k_mth': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r3m_bal10k_mth': [0, 'NaN', 'NaN', 3, 0, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'max_dp_dep_flag_12mth': [None, None, None, None, None, None, None, None, None, None],
    'r3m_bal5k_mth': [0, 'NaN', 'NaN', 3, 1, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'tot_dp_bal': [118, 'NaN', 'NaN', 23888.82, 6280, 'NaN', 'NaN', 'NaN', 'NaN', 32742],
    'r12m_dp_flg': [0, 'NaN', 'NaN', 1, 1, 'NaN', 'NaN', 'NaN', 'NaN', 1],
    'r3m_bal30k_mth': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'r6m_bal10k_mth': [0, 'NaN', 'NaN', 6, 0, 'NaN', 'NaN', 'NaN', 'NaN', 6],
    'r3m_bal100k_mth': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r6m_bal30k_mth': [0, 'NaN', 'NaN', 3, 0, 'NaN', 'NaN', 'NaN', 'NaN', 6],
    'r3m_bal2k_mth': [0, 'NaN', 'NaN', 3, 1, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'sum_amt_out_wm_12mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'max_dp_insur_flag_12mth': [None, None, None, None, None, None, None, None, None, None],
    'sum_amt_out_cwd_12mth': ['NaN', 'NaN', 'NaN', 707000, 5000, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r6m_bal5k_mth': [0, 'NaN', 'NaN', 6, 2, 'NaN', 'NaN', 'NaN', 'NaN', 6],
    'r6m_bal2k_mth': [0, 'NaN', 'NaN', 6, 4, 'NaN', 'NaN', 'NaN', 'NaN', 6],
    'r3m_bal1k_mth': [0, 'NaN', 'NaN', 3, 3, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'sum_amt_out_wm_6mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r12m_dpfx_flg': [0, 'NaN', 'NaN', 1, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r12m_bal5k_mth': [0, 'NaN', 'NaN', 12, 2, 'NaN', 'NaN', 'NaN', 'NaN', 12],
    'r12m_bal1k_mth': [0, 'NaN', 'NaN', 12, 7, 'NaN', 'NaN', 'NaN', 'NaN', 12],
    'pct_amt_out_wm_12mth_ln': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_out_cwd_6mth': ['NaN', 'NaN', 'NaN', 36000, 5000, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_txn_month_w_debtpay_12mth': ['NaN', 'NaN', 'NaN', 12, 8, 'NaN', 'NaN', 'NaN', 'NaN', 2],
    'amt_out_wm_per_12mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r12m_salary_flg': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r6m_bal1k_mth': [0, 'NaN', 'NaN', 6, 6, 'NaN', 'NaN', 'NaN', 'NaN', 6],
    'r1m_security_flg': [1, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r6m_dp_flg': [0, 'NaN', 'NaN', 1, 1, 'NaN', 'NaN', 'NaN', 'NaN', 1],
    'sum_amt_dp_salary_6mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'pct_cnt_out_wm_12mth_ln': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r3m_tot_dp_bal': [118, 'NaN', 'NaN', 22009.333333, 3135.333333, 'NaN', 'NaN', 'NaN', 'NaN', 32742],
    'r12m_bal2k_mth': [0, 'NaN', 'NaN', 12, 5, 'NaN', 'NaN', 'NaN', 'NaN', 12],
    'sum_cnt_out_n_wm_3mth': ['NaN', 'NaN', 'NaN', 6, 2, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r3m_bal20k_mth': [0, 'NaN', 'NaN', 2, 0, 'NaN', 'NaN', 'NaN', 'NaN', 3],
    'sum_cnt_out_cwd_6mth': ['NaN', 'NaN', 'NaN', 6, 1, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r12m_bal20k_mth': [0, 'NaN', 'NaN', 11, 0, 'NaN', 'NaN', 'NaN', 'NaN', 12],
    'sum_amt_in_oth_12mth': ['NaN', 'NaN', 'NaN', 26071, 29884, 'NaN', 'NaN', 'NaN', 'NaN', 10],
    'sum_cnt_out_w_debtpay_12mth': ['NaN', 'NaN', 'NaN', 39, 9, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_cnt_out_cwd_12mth': ['NaN', 'NaN', 'NaN', 18, 1, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_txn_month_w_debtpay_3mth': ['NaN', 'NaN', 'NaN', 3, 3, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'amt_out_ln_pay_per_3mth': ['NaN', 'NaN', 'NaN', 922, 3885.666667, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r3m_aum_bal': [118, 'NaN', 'NaN', 30414, 2344.666667, 'NaN', 'NaN', 'NaN', 'NaN', 32740.666667],
    'amt_out_w_debtpay_per_12mth': ['NaN', 'NaN', 'NaN', 21620.897436, 2611.555556, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_out_oth_12mth': ['NaN', 'NaN', 'NaN', 90230, 1903, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_in_cash_12mth': ['NaN', 'NaN', 'NaN', 373900, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_in_tfr_12mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_cnt_out_n_wm_6mth': ['NaN', 'NaN', 'NaN', 11, 3, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_cnt_total_w_debtpay_12mth': ['NaN', 'NaN', 'NaN', 58, 19, 'NaN', 'NaN', 'NaN', 'NaN', 2],
    'sum_cnt_out_loan_pay_3mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_cnt_out_loan_pay_6mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_in_fx_12mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_cnt_out_ln_pay_6mth': ['NaN', 'NaN', 'NaN', 6, 6, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r12m_aum_bal': [118, 'NaN', 'NaN', 156377.583333, 1579, 'NaN', 'NaN', 'NaN', 'NaN', 32736.5],
    'sum_cnt_out_w_debtpay_6mth': ['NaN', 'NaN', 'NaN', 17, 9, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'sum_amt_out_loan_pay_12mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r6m_salary_flg': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'salary_flag': [0, 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'r1m_dpfx_flg': [0, 'NaN', 'NaN', 1, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'max_dp_dep_flag_6mth': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_fail_txn_pct_12m': ['NaN', 'NaN', 'NaN', 0, 5, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_fail_txn_amt_12m': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_ins_txn_diff_amt_12m': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_txn_amt_1m': ['NaN', 'NaN', 'NaN', 14156, 19246, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_ins_txn_amt_12m': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_txn_amt_3m': ['NaN', 'NaN', 'NaN', 52630, 34060, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'dp_ins_txn_cnt_12m': ['NaN', 'NaN', 'NaN', 0, 0, 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'ins_l_pre_amt_ever': ['NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'],
    'ins_n_pre_amt_ever': ['NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'],
    'svc_m1p_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'unsec_wst_delq_3m': ['M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0'],
    'unsec_m1p_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'svc_wst_delq_12m': ['M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0', 'M0'],
    'cc_m1p_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_use_pct': [4, 2, 0, 1, 7, 1, 1, 100, 4, 0],
    'cc_revolv_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'svc_prod_m1p_cnt_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_pct': [4, 2, 0, 1, 7, 1, 1, 100, 4, 0],
    'svc_m2p_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_revolv_pct_12m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_revolv_cnt_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_live_mob': [23.2, 101.5, 80.3, 116.6, 7, 58.8, 113.2, 15.6, 66.7, 163.2],
    'pl_amt': [None, None, None, None, None, None, None, None, None, None],
    'svc_mob': [23.2, 101.5, 80.3, 116.6, 7, 58.8, 113.2, 15.6, 66.7, 163.2],
    'unsec_bal': [18961, 6954, 0, 1292, 4246, 1683, 2892, 30991, 4101, 0],
    'cc_amt': [430000, 300000, 150000, 130000, 60000, 150000, 200000, 30000, 100000, 250000],
    'svc_bal': [18961, 6954, 0, 1292, 4246, 1683, 2892, 30991, 4101, 0],
    'cc_pct_3m': [4, 5, 0, 1, 6, 2, 3, 98, 4, 0],
    'svc_prod_m2p_cnt_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_revolv_pct_p30_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_pct_p30_12m': [0, 0, 0, 0, 0, 0, 0, 12, 0, 0],
    'cc_pct_p50_12m': [0, 0, 0, 0, 0, 0, 0, 10, 0, 0],
    'pl_pct': [None, None, None, None, None, None, None, None, None, None],
    'pl_bal': [None, None, None, None, None, None, None, None, None, None],
    'cc_revolv_cepp_cnt_3m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_bal_12m': [8616.25, 16316.83, 95.83, 6910.25, 2371.57, 2539.08, 2923.58, 22461.67, 4387.92, 0],
    'svc_amt': [430000, 300000, 150000, 130000, 60000, 150000, 200000, 30000, 100000, 250000],
    'cc_revolv_pct_p50_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'cc_auth_fail_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_auth_fail_cnt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_auth_fail_pwd_amt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_auth_fail_cnt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_auth_amt_3m': [53693, 30394, 0, 5487, 10493, 3208, 17834, 86422, 14421, 0],
    'cc_auth_fail_block_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_auth_ec_amt_3m': [0, 0, 'NaN', 0, 9743, 0, 7058, 83988, 0, 'NaN'],
    'cc_auth_manual_voice_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 13.87, 'NaN'],
    'cc_auth_not_force_cnt_3m': [9, 9, 'NaN', 17, 16, 2, 13, 6, 17, 'NaN'],
    'cc_auth_oversea_cnt_pct_3m': [0, 0, 'NaN', 0, 100, 0, 0, 0, 0, 'NaN'],
    'cc_auth_cnt_3m': [9, 9, 0, 17, 16, 2, 13, 6, 17, 0],
    'cc_auth_normal_cnt_3m': [9, 9, 'NaN', 17, 0, 2, 5, 3, 5, 'NaN'],
    'cc_auth_fail_insuf_cr_cnt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn02b_cnt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn03_amt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn14_amt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 85638, 1600, 'NaN'],
    'cc_txn02b_amt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn18_amt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn01c_amt_3m': [48647, 30923, 'NaN', 2569, 0, 3208, 5680, 0, 0, 'NaN'],
    'cc_txn23_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn14_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 99.09, 15.72, 'NaN'],
    'cc_txn_amt_3m': [48647, 30923, 0, 2569, 5843, 3208, 13708, 86422, 10178, 0],
    'cc_txn06_cnt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 16.67, 66.67, 'NaN'],
    'cc_txn_cnt_3m': [8, 9, 0, 9, 9, 2, 9, 6, 12, 0],
    'cc_txn04_amt_pct_3m': [0, 0, 'NaN', 0, 86.65, 0, 0, 0, 0, 'NaN'],
    'cc_txn06a_cnt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 16.67, 66.67, 'NaN'],
    'cc_txn05b_amt_pct_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_txn02b_cnt_3m': [0, 0, 'NaN', 0, 0, 0, 0, 0, 0, 'NaN'],
    'cc_apply_last_product_3y': ['1.NEW_CARD', None, None, None, '1.NEW_CARD', None, None, '1.NEW_CARD', None, None],
    'cc_apply_last_chnl_3y': ['4.DS', None, None, None, '1.BRANCH', None, None, '4.DS', None, None],
    'cc_apply_last_salary_type_3y': ['2.ASSETS', None, None, None, '1.SALARY', None, None, '1.SALARY', None, None],
    'cc_apply_risk_career_3y': [1, 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 0, 'NaN', 'NaN'],
    'cc_apply_last_card_3y': ['3.PT', None, None, None, '4.SP', None, None, '3.PT', None, None],
    'cc_apply_cash_cnt_pct_3y': [0, 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 0, 'NaN', 'NaN'],
    'cc_apply_p_non_ow_cnt_pct_3y': [100, 'NaN', 'NaN', 'NaN', 100, 'NaN', 'NaN', 50, 'NaN', 'NaN'],
    'cc_apply_r_cnt_pct_3y': [0, 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 50, 'NaN', 'NaN'],
    'pl_apply_flag_3y': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'pl_apply_r_cnt_pct_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_chnl_nondigit_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_r_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_rr5_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_source_nontm_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_funding_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_bt_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_p_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'pl_apply_ncxl_tmr_cnt_3y': [None, None, None, None, None, None, None, None, None, None],
    'ml_apply_collat_avg_age': [None, None, None, None, None, None, None, None, None, None],
    'e99_cnt': [16, 0, 8, 1, 6, 5, 2, 18, 0, 9],
    'mindist_fisherassn': [27017.359016, 22426.871448, 19556.140312, 27198.045751, 16795.669777, 16071.906184, 24183.055325, 5148.519276, 19474.214444, 22440.306395],
    'fld03': [1079, 783, 1041, 1317, 1025, 889, 1093, 842, 1034, 776],
    'rds500_bus': [24, 44, 111, 164, 165, 97, 249, 61, 72, 111],
    'tdp_txn_aum_flag_per': ['NaN', 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'],
    'family_bad_tags_per': ['NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 0],
    'family_aum_flag_per': ['NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 'NaN', 0.666667],
    'tdp_txn_vip_flag_cc_per': ['NaN', 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'],
    'tdp_txn_vip_flag_wm_per': ['NaN', 'NaN', 'NaN', 'NaN', 0, 'NaN', 'NaN', 'NaN', 'NaN', 'NaN'],
    'jcic_m3_flag_12m': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    'jcic_m2_flag_6m': [0, 0, 0, 0, 1, 1, 0, 0, 0, 0],
    'jcic_m2_flag_12m': [0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
    'ln_jcic_m2p_cnt_6m': [0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    'jcic_m2_2cnt_6m': [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
    'jcic_m2_1cnt_12m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'vam106107_flag_12m': [0, 0, 0, 1, 0, 1, 0, 0, 0, 0],
    'cc_jcic_m2p_cnt_12m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'jcic_m2_1cnt_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'ln_jcic_m3p_cnt_12m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'all_wst_delq_6m': ['OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS'],
    'cc_jcic_m2p_cnt_6m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    'all_wst_delq_12m': ['OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS', 'OTHERS'],
    'ln_jcic_m2p_cnt_12m': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
 })

