from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import asyncpg


def get_engine(user, passwd, host, port, db):
    url = f"postgresql://{user}:{passwd}@{host}:{port}/{db}"
    # if not database_exists(url):
    #     create_database(url)
    engine = create_engine(url, pool_size=50, echo=False)
    return engine


def get_session(user, passwd, host, port, db):
    """
    session = get_session()
    session.close()
    """
    engine = get_engine(user, passwd, host, port, db)
    session = sessionmaker(bind=engine)()
    return session
