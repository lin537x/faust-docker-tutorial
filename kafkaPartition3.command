
# https://stackoverflow.com/questions/37960767/how-to-change-the-number-of-replicas-of-a-kafka-topic
kafka-topics.sh --alter --bootstrap-server kafka1:9092,kafka2:9092 --topic SCR-SCORING-DATR-M001SHIELDREQ --partitions 3
kafka-topics.sh --alter --bootstrap-server kafka1:9092,kafka2:9092 --topic SCR-SCORING-DATR-M001SHIELDRPL --partitions 3
